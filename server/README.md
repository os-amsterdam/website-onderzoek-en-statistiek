# Docker constellation and domains

## Docker constellation
Using a GitLab CI/CD pipeline, frontend and backend of the website Onderzoek en statistiek are deployed to a Virtual Private Server in a Docker constellation consisting of these containers and volumes

### Server
* container `os-server`
* volume `server_nginx_secrets`

### Acceptance environment
* container `os-cms-acc`
* container `os-db-acc`
* container `os-website-acc`
* volume `acceptance_cms_data`
* volume `acceptance_cms_files`
* volume `acceptance_static_files`

### Production environment
* container `os-cms-prod`
* container `os-db-prod`
* container `os-website-prod`
* volume `production_cms_data`
* volume `production_cms_files`
* volume `production_static_files`

## Domains

The Nginx server in the `os-server` container handles these (sub)domains

### Acceptance environment
* `acc.onderzoek.amsterdam.nl` => acceptance website
* `acc.onderzoek.amsterdam.nl/static` => static content acceptance
* `acc.cms.onderzoek-en-statistiek.nl` => acceptance cms

### Production environment
* `onderzoek.amsterdam.nl` => production website
* `statistiek.amsterdam.nl` => production website
* `onderzoek.amsterdam.nl/static` => static content production
* `statistiek.amsterdam.nl/static` => static content production
* `cms.onderzoek-en-statistiek.nl` => production cms

## Diagram

![Diagram docker constellation and domains](./docker-constellation-and-domains.png)