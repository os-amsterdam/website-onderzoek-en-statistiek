# Website Onderzoek en statistiek

This repository contains all the code neccessary for the backend and frontend of the website onderzoek.amsterdam.nl. The code base is separated in:

- `/server`, containing the configuration for a Nginx server
- `/cms`, containing the code for an instance of the Strapi content management system
- `/website`, containing the code for the Next.js frontend
- `/tools`, some tools for maintenance of the website

## Development

### Prerequisites

- nodejs
- npm
- docker
- docker compose

### Recommendations

We use ESLint and Prettier in this repository, so it's strongly advised to use ESLint and Prettier plugins for your editor/IDE

### Scripts

| script       | description                                                                                  |
| ------------ | -------------------------------------------------------------------------------------------- |
| dev          | spin up docker image and run development versions of the cms and website                     |
| postinstall  | runs automatically after `npm install` in the root and installs cms and website dependencies |
| server:check | check NGINX configuration                                                                    |

### CMS

- Start local database: `npm run cms:db`
- Open a new terminal and install cms dependencies: `npm run cms:install`
- Start local cms: `npm run cms`
- Start with creating an admin account at <http://localhost:1337/admin>
- Run some tests with `npm run cms:test`, `npm run cms:test acc` or `npm run cms:test prod`
- After making changes to the Strapi admin frontend (like the positioning of fields, or adding info texts), be sure to run `npm run config-dump` in the `/cms` folder and check in the new Strapi config file

## Further documentation

- [Docker constellation and domains](server/README.md)
- [Front-end](website/README.md)
