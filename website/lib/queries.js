const bodyQuery = {
  on: {
    'shared.text-with-links': {
      // populate: '*',
      // populating specific fields is SLOWER then populating everything in Next 15.1.4!
      // we need to filter for publishedAt to be null to prevent draft documents to show up
      // this is a bug: https://github.com/strapi/strapi/issues/22456
      populate: {
        articles: {
          filters: { publishedAt: { $notNull: true } },
          fields: ['title', 'shortTitle', 'slug'],
        },
        publications: {
          filters: { publishedAt: { $notNull: true } },
          fields: ['title', 'shortTitle', 'slug'],
        },
        datasets: {
          filters: { publishedAt: { $notNull: true } },
          fields: ['title', 'slug'],
        },
        videos: {
          filters: { publishedAt: { $notNull: true } },
          fields: ['title', 'shortTitle', 'slug'],
        },
        interactives: {
          filters: { publishedAt: { $notNull: true } },
          fields: ['title', 'shortTitle', 'slug'],
        },
        collections: {
          filters: { publishedAt: { $notNull: true } },
          fields: ['title', 'shortTitle', 'slug'],
        },
        links: {
          fields: ['id', 'title', 'path'],
        },
      },
    },
    'shared.text': { populate: '*' },
    'shared.text-box': { populate: '*' },
    'shared.visualisation': { populate: '*' },
    'shared.panel-group': {
      populate: {
        panel: { populate: '*' },
      },
    },

    'shared.embed': { populate: '*' },
    'shared.body-video': { populate: '*' },
  },
}

const featuredQuery = {
  on: {
    'shared.articles': {
      populate: {
        articles: {
          fields: ['title', 'shortTitle', 'slug', 'teaser'],
          populate: {
            rectangularImage: {
              fields: ['url'],
            },
          },
        },
      },
    },
    'shared.publications': {
      populate: {
        publications: {
          fields: ['title', 'shortTitle', 'slug', 'teaser'],
          populate: {
            rectangularImage: {
              fields: ['url'],
            },
          },
        },
      },
    },
    'shared.videos': {
      populate: {
        videos: {
          fields: ['title', 'shortTitle', 'slug', 'teaser'],
          populate: {
            rectangularImage: {
              fields: ['url'],
            },
          },
        },
      },
    },
    'shared.collections': {
      populate: {
        collections: {
          fields: ['title', 'shortTitle', 'slug', 'teaser'],
          populate: {
            rectangularImage: {
              fields: ['url'],
            },
          },
        },
      },
    },
    'shared.interactives': {
      populate: {
        interactives: {
          fields: ['title', 'shortTitle', 'slug', 'teaser'],
          populate: {
            rectangularImage: {
              fields: ['url'],
            },
          },
        },
      },
    },
    'shared.datasets': {
      populate: {
        datasets: {
          fields: ['title', 'slug', 'teaser'],
          populate: {
            rectangularImage: {
              fields: ['url'],
            },
          },
        },
      },
    },
  },
}

export const homepageQuery = {
  populate: {
    featured: featuredQuery,
    featuredCollections: {
      populate: {
        collections: {
          fields: ['title', 'shortTitle', 'slug', 'teaser'],
        },
      },
    },
    agenda: {
      on: {
        'shared.articles': {
          populate: {
            articles: { fields: ['title', 'shortTitle', 'slug'] },
          },
        },
        'shared.videos': {
          populate: {
            videos: { fields: ['title', 'shortTitle', 'slug'] },
          },
        },
      },
    },
    shortcuts: {
      on: {
        'shared.articles': {
          populate: {
            articles: { fields: ['title', 'shortTitle', 'slug'] },
          },
        },
        'shared.videos': {
          populate: {
            videos: { fields: ['title', 'shortTitle', 'slug'] },
          },
        },
        'shared.collections': {
          populate: {
            collections: { fields: ['title', 'shortTitle', 'slug'] },
          },
        },
        'shared.datasets': {
          populate: {
            datasets: { fields: ['title', 'slug'] },
          },
        },
        'shared.interactives': {
          populate: {
            interactives: { fields: ['title', 'shortTitle', 'slug'] },
          },
        },
        'shared.publications': {
          populate: {
            publications: { fields: ['title', 'shortTitle', 'slug'] },
          },
        },
      },
    },
    relatedSites: { populate: '*' },
  },
}

export const themesQuery = { fields: ['title', 'slug'] }

export const themeQuery = {
  populate: {
    rectangularImage: {
      fields: ['url'],
    },
    visualisation: {
      populate: {
        image: {
          fields: ['url', 'width', 'height'],
        },
      },
    },
    topStory: {
      on: {
        'shared.articles': {
          populate: {
            articles: { fields: ['slug'] },
          },
        },
        'shared.interactives': {
          populate: {
            interactives: { fields: ['slug'] },
          },
        },
        'shared.publications': {
          populate: {
            publications: { fields: ['slug'] },
          },
        },
        'shared.videos': {
          populate: {
            videos: { fields: ['slug'] },
          },
        },
      },
    },
    featured: featuredQuery,
    collections: {
      fields: ['title', 'slug', 'updatedAt'],
    },
    combiPicture: {
      populate: {
        smallerImage: {
          fields: ['url'],
        },
        biggerImage: {
          fields: ['url'],
        },
      },
    },
  },
}

export const articleQuery = {
  populate: {
    rectangularImage: {
      fields: ['url', 'caption', 'width', 'height'],
    },
    body: bodyQuery,
    related: { populate: '*' },
    themes: themesQuery,
  },
}

export const publicationQuery = {
  populate: {
    file: {
      fields: ['name', 'url', 'size'],
    },
    coverImage: {
      fields: ['alternativeText', 'url', 'caption', 'width', 'height'],
    },
    body: bodyQuery,
    themes: themesQuery,
  },
}

export const videoQuery = {
  populate: {
    rectangularImage: {
      fields: ['url', 'caption', 'width', 'height'],
    },
    videoFile: {
      fields: ['url'],
    },
    subtitleFile: {
      fields: ['url'],
    },
    body: bodyQuery,
    themes: themesQuery,
  },
}

export const interactiveQuery = {
  populate: {
    rectangularImage: {
      fields: ['url'],
    },
    themes: themesQuery,
  },
}

export const collectionQuery = {
  populate: {
    body: bodyQuery,
    featured: featuredQuery,
    collectionItems: { populate: '*' },
    linkList: { populate: '*' },
    themes: themesQuery,
  },
}

export const datasetQuery = {
  populate: {
    rectangularImage: {
      fields: ['url', 'caption', 'width', 'height'],
    },
    body: bodyQuery,
    resources: {
      populate: {
        file: { fields: ['url', 'ext', 'size'] },
      },
    },
    themes: themesQuery,
  },
}
