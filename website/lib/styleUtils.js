// concats classnames
export const cx = (...args) =>
  args
    .flat()
    .filter((x) => typeof x === 'string')
    .join(' ')
    .trim()

// converts an array of react props to an object with css vars
// for example: props [ { height: 20 }, { width: { small: 4, large: 8} }, { color: '#ff0000' } ]
// returns { '--height': 20px, '--width': 4px, '--width-lg': 8px, --color: '#ff0000' }
export const propsToCssVars = (props) =>
  props.reduce((acc, next) => {
    const [key, val] = Object.entries(next)[0]
    const cssVarKey = `--${key}`
    const cssVarKeyLarge = `--${key}-lg`
    const obj = { [cssVarKey]: val }
    if (typeof val === 'number') obj[cssVarKey] = `${val}px`
    if (typeof val === 'object') {
      if (val?.small) obj[cssVarKey] = `${val.small}px`
      if (val?.large) obj[cssVarKeyLarge] = `${val.large}px`
    }
    return { ...acc, ...obj }
  }, {})

export const getFontColor = (backgroundColor) => {
  if (!backgroundColor) return null
  if (backgroundColor === 'paars' || backgroundColor === 'roze') return 'white'
  return 'black'
}

export const translateColor = (name) => {
  const translations = {
    grijs: 'var( --grey)',
    wit: 'var( --white)',
    lichtblauw: 'var(--light-blue)',
    blauw: 'var(--light-blue)',
    groen: 'var(--dark-green)',
    lichtgroen: 'var(--green)',
    oranje: 'var(--orange)',
    roze: 'var(--magenta)',
    paars: 'var(--purple)',
    geel: 'var(--yellow)',
  }
  return translations[name]
}
