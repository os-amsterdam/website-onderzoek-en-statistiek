import { useEffect, useState } from 'react'

const breakpoint = '839px'

const useIsMobile = () => {
  const mediaQuery = `(max-width: ${breakpoint})`

  const [isMatch, setIsMatch] = useState(
    mediaQuery
      ? typeof window !== 'undefined' && window.matchMedia(mediaQuery).matches
      : true,
  )

  const handleMediaQueryListEvent = ({ matches }) => setIsMatch(matches)

  useEffect(() => {
    let matchMedia
    if (typeof window !== 'undefined') {
      matchMedia = window.matchMedia(mediaQuery)
      handleMediaQueryListEvent(matchMedia)

      // addEventListener might throw an error on iOS / Safari
      if (matchMedia.addEventListener) {
        matchMedia.addEventListener('change', handleMediaQueryListEvent)
      } else {
        matchMedia.addListener(handleMediaQueryListEvent)
      }
    }
    return () => {
      if (matchMedia && typeof window !== 'undefined') {
        // removeEventListener might throw an error on iOS / Safari
        if (matchMedia.removeEventListener) {
          matchMedia.removeEventListener('change', handleMediaQueryListEvent)
        } else {
          matchMedia.removeListener(handleMediaQueryListEvent)
        }
      }
    }
  }, [])

  return isMatch
}

export default useIsMobile
