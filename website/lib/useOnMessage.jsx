import { useEffect } from 'react'

import ALLOWED_DOMAINS from '../constants/allowedDomains'

export default function useOnMessage(handler) {
  useEffect(() => {
    function onMessage({ origin, data }) {
      // don't do anything with the event if it's not whitelisted
      if (
        !(
          (origin.startsWith('http://localhost') ||
            origin.startsWith('http://127.0.0.1')) &&
          process.env.NODE_ENV === 'development'
        ) &&
        !ALLOWED_DOMAINS.find((domain) => origin.startsWith(domain))
      ) {
        return
      }

      handler(data)
    }

    window.addEventListener('message', onMessage)

    return () => window.removeEventListener('message', onMessage)
  })
}
