# Website Onderzoek en Statistiek front-end

This directory contains all code for the front-end of the [Onderzoek en Statistiek website](https://onderzoek.amsterdam.nl/).

## Stack

- Framework/UI: Next.js and React
- Styling: plain CSS and CSS Modules
- Linting: ESLint and Prettier
- Testing: Jest and React Testing Library

All page types of the website can be found under `/pages`. The pages almost always use React components, which you can find in `/components`. Custom functions and hooks can be found in `/lib` and all reusable constants in `/constants`.

## Styling

- Most of the styling depends on some plain CSS files used by all O&S projects. More information on [O&S Style](https://onderzoek.amsterdam.nl/static/style/)
- In the `/styles` directory, we define some global CSS.
- Additional styling on the level of pages and components is handled by CSS Modules.
- We write plain CSS (no SASS, LESS et cetera). Nesting is allowed.
- For relatively simple components, we aim for a single class name, `.style`.
- For more complex components, we use multiple classes in camelCase. This makes it easier to reference them in JavaScript (`styles.searchLink` instead of `styles['search-link']`).
- We use two methods to pass variables to the CSS:
  1. Booleans and strings are passed through a data attribute: `data-is-open`, `data-variant="inline"`. We use kebab-case for data attributes.
  2. Numeric variables are passed as inline styles using a CSS variable: `style={{ '--size': '24px' }}`.
- The file `lib/styleUtils.js` contains helper functions for combining class names (`cx`) and converting React props into custom CSS variables (`propsToCssVars`).

## Getting started

- Install dependencies: `npm install`
- Start dev server with acceptance database: `npm run dev:acc`

## Scripts

| script          | description                                                                                  |
| --------------- | -------------------------------------------------------------------------------------------- |
| build           | build the website with a local database (or a database in another container in the pipeline) |
| build:container | build the website in a local container with the acceptance database                          |
| build:acc       | build the website with the acceptance database                                               |
| build:prod      | build the website with the production database                                               |
| dev             | run a development version of the website with a local database                               |
| dev:acc         | run a development version of the website with the acceptance database                        |
| dev:prod        | run a development version of the website with the production database                        |
| lint            | run ESLint                                                                                   |
| lint:fix        | run ESLint and automatically try to fix any problems                                         |
| start           | start a built version of the website                                                         |
| test            | run unit tests                                                                               |
| knip            | check for unused files, dependencies and exports                                             |

## Branch naming and merge requests

When creating a new branch, preferably branch off `develop`. Rebase on `develop` and squash commits when starting a merge request.

## Browser support

The targeted browsers for the website can be found in the `package.json` `browserslist`.

## Further documentation

- [GridItem component](components/GridItem/README.md)
