import useDownload from '../../lib/useDownload'
import { pushCustomEvent } from '../../lib/analyticsUtils'

import Icon from '../Icon/Icon'

const DownloadButton = ({
  children,
  url,
  type = 'publication',
  ...otherProps
}) => {
  const [, loading, downloadFile] = useDownload()

  return (
    <button
      type="button"
      {...otherProps}
      onClick={() => {
        pushCustomEvent('Download', type, url.split('/').pop())
        downloadFile(url, {}, url.split('/').pop())
      }}
    >
      {loading ? (
        <Icon name="spinner" className="mr-12" />
      ) : (
        <Icon name="download" className="mr-12" size={20} />
      )}
      {children}
    </button>
  )
}

export default DownloadButton
