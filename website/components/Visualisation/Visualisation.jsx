import slugify from 'slugify'
import { cx } from '../../lib/styleUtils'
import { prependStrapiURL } from '../../lib/utils'

import BodyBlock from '../BodyBlock/BodyBlock'

import styles from './Visualisation.module.css'

const createPanelGroupSpec = (
  spec,
  selectorLabel,
  selectorType,
  transitionType,
) => ({
  selectLabel: selectorLabel || 'Toon',
  selectorType,
  transition: transitionType,
  panels: spec.map(({ specification, label, image, source, altText }) => ({
    heading: label,
    spec: specification,
    imageUrl: prependStrapiURL(image?.url),
    source,
    altText,
  })),
})

const Visualisation = ({
  className,
  type,
  title,
  text,
  source,
  variant,
  color,
  altText,
  caption,
  image,
  specification,
  selectorType,
  selectorLabel,
  transitionType,
  panel: panels,
}) => {
  const spec =
    type === 'panel-group'
      ? createPanelGroupSpec(
          panels,
          selectorLabel,
          selectorType,
          transitionType,
        )
      : specification

  return (
    <BodyBlock variant={variant} color={color} text={text}>
      <div className={cx(styles.style, className)}>
        <os-visualisation
          heading={title}
          id={slugify(title)}
          spec={spec && JSON.stringify(spec)}
          imageUrl={prependStrapiURL(image?.url)}
          source={source}
          altText={altText}
          caption={caption}
        ></os-visualisation>
      </div>
    </BodyBlock>
  )
}

export default Visualisation
