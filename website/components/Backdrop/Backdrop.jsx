import styles from './Backdrop.module.css'

// Default z-index for backdrop is 500, so it's below the sticky header (z-index 501) and above all other content
// The zIndex prop enables a backdrop above the header (for example when the mobile search filters panel is open)

const Backdrop = ({ isOpen, zIndex = 500, onClick }) => {
  const handleOnClick = (e) => {
    if (onClick) {
      onClick(e)
    }
  }

  return (
    <div
      className={styles.style}
      style={{ zIndex }}
      data-is-open={isOpen || null}
      onClick={handleOnClick}
      role="button"
      aria-label="sluit menu"
    />
  )
}

export default Backdrop
