import NextLink from 'next/link'

const Link = ({
  href,
  as,
  external,
  className,
  darkBackground,
  children,
  variant,
  ...otherProps
}) => {
  return external ? (
    <a
      className={className}
      data-variant={variant}
      data-inverted={darkBackground}
      href={href}
      {...otherProps}
    >
      {children}
    </a>
  ) : (
    <NextLink
      className={className}
      data-variant={variant}
      data-inverted={darkBackground}
      href={href}
      as={as}
      prefetch={false}
      {...otherProps}
    >
      {children}
    </NextLink>
  )
}

export default Link
