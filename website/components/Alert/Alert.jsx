import styles from './Alert.module.css'

const Alert = ({
  variant = 'warning',
  text = 'Dit is een preview, deze pagina is nog niet gepubliceerd!',
  title,
}) => (
  <div className={styles.style} data-variant={variant}>
    {title && <h2 data-style-as="h5">{title}</h2>}
    <p>{text}</p>
  </div>
)

export default Alert
