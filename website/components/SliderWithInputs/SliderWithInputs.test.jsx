import { fireEvent, render, screen } from '@testing-library/react'

import SliderWithInputs from './SliderWithInputs'

describe('SliderWithInputs component', () => {
  const Component = ({ onChange = jest.fn(), ...restProps }) => (
    <SliderWithInputs
      totalRange={[1900, 2000]}
      selectedRange={[1990, 1990]}
      onChange={onChange}
      {...restProps}
    />
  )

  it('renders', () => {
    const { container } = render(<Component />)

    const sliderWithInputs = container.querySelector(':only-child')

    expect(sliderWithInputs).toBeInTheDocument()
    expect(sliderWithInputs).toBeVisible()
  })

  it('renders an additional class name', () => {
    const { container } = render(<Component className="extra" />)

    const component = container.querySelector(':only-child')

    expect(component).toHaveClass('extra')
  })

  describe('Slider', () => {
    it('sets max value to min value, when min value is higher than max value', () => {
      render(<Component />)

      const minSlider = screen.getByRole('slider', { name: 'Begin jaar' })
      const maxSlider = screen.getByRole('slider', { name: 'Eind jaar' })

      fireEvent.change(minSlider, { target: { value: 1995 } })

      expect(maxSlider.value).toBe('1995')
    })
    it('sets min value to max value, when max value is lower than min value', () => {
      render(<Component />)

      const minSlider = screen.getByRole('slider', { name: 'Begin jaar' })
      const maxSlider = screen.getByRole('slider', { name: 'Eind jaar' })

      fireEvent.change(maxSlider, { target: { value: 1980 } })

      expect(minSlider.value).toBe('1980')
    })
  })

  describe('NumBoxes', () => {
    it('allows max 4 characters', () => {
      render(<Component />)

      const minNumBox = screen.getByRole('spinbutton', { name: 'Begin jaar' })
      const maxNumBox = screen.getByRole('spinbutton', { name: 'Eind jaar' })

      fireEvent.change(minNumBox, { target: { value: 1912 } })
      fireEvent.change(minNumBox, { target: { value: 19123 } })
      fireEvent.change(maxNumBox, { target: { value: 1923 } })
      fireEvent.change(maxNumBox, { target: { value: 19234 } })

      expect(minNumBox.value).toBe('1912')
      expect(maxNumBox.value).toBe('1923')
    })

    it('does not call onChange with less than 4 chars', () => {
      const handleChange = jest.fn()
      render(<Component onChange={handleChange} />)

      const minNumBox = screen.getByRole('spinbutton', { name: 'Begin jaar' })
      const maxNumBox = screen.getByRole('spinbutton', { name: 'Eind jaar' })

      fireEvent.change(minNumBox, { target: { value: 1 } })
      fireEvent.change(minNumBox, { target: { value: 1912 } })
      fireEvent.change(maxNumBox, { target: { value: 2 } })
      fireEvent.change(maxNumBox, { target: { value: 1923 } })

      expect(handleChange).toHaveBeenCalledTimes(2)
    })

    it('returns total range values when values are outside range', () => {
      render(<Component />)

      const minNumBox = screen.getByRole('spinbutton', { name: 'Begin jaar' })
      const maxNumBox = screen.getByRole('spinbutton', { name: 'Eind jaar' })

      fireEvent.change(minNumBox, { target: { value: 1800 } })
      fireEvent.change(maxNumBox, { target: { value: 2020 } })

      expect(minNumBox.value).toBe('1900')
      expect(maxNumBox.value).toBe('2000')
    })

    it('returns total range values on blur when values are invalid', () => {
      render(<Component />)

      const minNumBox = screen.getByRole('spinbutton', { name: 'Begin jaar' })
      const maxNumBox = screen.getByRole('spinbutton', { name: 'Eind jaar' })

      fireEvent.change(minNumBox, { target: { value: 1 } })
      fireEvent.blur(minNumBox)

      expect(minNumBox.value).toBe('1900')

      fireEvent.change(maxNumBox, { target: { value: 22 } })
      fireEvent.blur(maxNumBox)

      expect(maxNumBox.value).toBe('2000')
    })

    it('sets max value to min value, when min value is higher than max value', () => {
      render(<Component />)

      const minNumBox = screen.getByRole('spinbutton', { name: 'Begin jaar' })
      const maxNumBox = screen.getByRole('spinbutton', { name: 'Eind jaar' })

      fireEvent.change(minNumBox, { target: { value: 1995 } })

      expect(maxNumBox.value).toBe('1995')
    })

    it('sets min value to max value, when max value is lower than min value', () => {
      render(<Component />)

      const minNumBox = screen.getByRole('spinbutton', { name: 'Begin jaar' })
      const maxNumBox = screen.getByRole('spinbutton', { name: 'Eind jaar' })

      fireEvent.change(maxNumBox, { target: { value: 1980 } })

      expect(minNumBox.value).toBe('1980')
    })
  })

  describe('Sync', () => {
    it('always shows the same value for slider and num box', () => {
      render(<Component />)

      const minSlider = screen.getByRole('slider', { name: 'Begin jaar' })
      const minNumBox = screen.getByRole('spinbutton', { name: 'Begin jaar' })
      const maxSlider = screen.getByRole('slider', { name: 'Eind jaar' })
      const maxNumBox = screen.getByRole('spinbutton', { name: 'Eind jaar' })

      fireEvent.change(minSlider, { target: { value: 1980 } })
      expect(minNumBox.value).toBe('1980')

      fireEvent.change(maxSlider, { target: { value: 2000 } })
      expect(maxNumBox.value).toBe('2000')

      fireEvent.change(minNumBox, { target: { value: 1970 } })
      expect(minSlider.value).toBe('1970')

      fireEvent.change(maxNumBox, { target: { value: 1999 } })
      expect(maxSlider.value).toBe('1999')
    })
  })
})
