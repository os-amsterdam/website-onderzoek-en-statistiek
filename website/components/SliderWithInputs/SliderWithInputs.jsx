import { useEffect, useState } from 'react'

import { isInRange } from '../../lib/searchUtils'
import { cx } from '../../lib/styleUtils'

import styles from './SliderWithInputs.module.css'

function checkMaxValueValidity({ minValue, maxValue }) {
  if (maxValue < minValue) {
    return { minValue: maxValue, maxValue }
  }

  return { minValue, maxValue }
}

function checkMinValueValidity({ minValue, maxValue }) {
  if (minValue > maxValue) {
    return { minValue, maxValue: minValue }
  }

  return { minValue, maxValue }
}

const SliderWithInputs = ({
  className,
  selectedRange,
  totalRange,
  onChange,
}) => {
  const [sliderValues, setSliderValues] = useState({
    minValue: selectedRange[0] || totalRange[0],
    maxValue: selectedRange[1] || totalRange[1],
  })

  const [numBoxValues, setNumBoxValues] = useState({
    minValue: selectedRange[0] || totalRange[0],
    maxValue: selectedRange[1] || totalRange[1],
  })

  const handleChange = (values) => {
    setSliderValues(values)
    setNumBoxValues(values)
    onChange(values)
  }

  const handleMinNumBoxChange = (value) => {
    const sanitizedValues = checkMinValueValidity({
      ...numBoxValues,
      // if value is outside of the total range, set the minimum value
      minValue: isInRange(value, totalRange) ? value : totalRange[0],
    })

    handleChange(sanitizedValues)
  }

  const handleMaxNumBoxChange = (value) => {
    const sanitizedValues = checkMaxValueValidity({
      ...numBoxValues,
      // if value is outside of the total range, set the maximum value
      maxValue: isInRange(value, totalRange) ? value : totalRange[1],
    })

    handleChange(sanitizedValues)
  }

  // this effect is needed for forward and back button behaviour
  useEffect(() => {
    if (selectedRange[0] && selectedRange[1]) {
      const values = {
        minValue: selectedRange[0],
        maxValue: selectedRange[1],
      }

      setSliderValues(values)
      setNumBoxValues(values)
    } else {
      const values = {
        minValue: totalRange[0],
        maxValue: totalRange[1],
      }

      setSliderValues(values)
      setNumBoxValues(values)
    }
  }, [selectedRange[0], selectedRange[1]])

  const min = totalRange[0]
  const max = totalRange[1]
  const trackVars = {
    '--min': `${((sliderValues.minValue - min) / (max - min)) * 100}%`,
    '--max': `${((sliderValues.maxValue - min) / (max - min)) * 100}%`,
  }

  return (
    <div className={cx(styles.container, className)}>
      <div className={styles.track} style={trackVars} />
      <input
        type="range"
        min={totalRange[0]}
        max={totalRange[1]}
        value={sliderValues.minValue}
        onChange={(e) => {
          const sanitizedValues = checkMinValueValidity({
            ...sliderValues,
            minValue: e.target.value,
          })

          handleChange(sanitizedValues)
        }}
        aria-labelledby="minValueLabel"
      />
      <input
        type="range"
        min={totalRange[0]}
        max={totalRange[1]}
        value={sliderValues.maxValue}
        onChange={(e) => {
          const sanitizedValues = checkMaxValueValidity({
            ...sliderValues,
            maxValue: e.target.value,
          })

          handleChange(sanitizedValues)
        }}
        aria-labelledby="maxValueLabel"
      />
      <div className={styles.box}>
        <label id="minValueLabel" htmlFor="minValue">
          Begin jaar
        </label>
        <input
          id="minValue"
          type="number"
          min={totalRange[0]}
          max={totalRange[1]}
          value={numBoxValues.minValue}
          onBlur={(e) => handleMinNumBoxChange(e.target.value)}
          onChange={(e) => {
            const { value } = e.target
            // don't allow values larger than 4 chars
            if (value.length > 4) return

            // if value is exactly 4 chars, kick off validation
            if (value.length === 4) {
              handleMinNumBoxChange(value)
              return
            }

            setNumBoxValues({
              ...numBoxValues,
              minValue: value,
            })
          }}
        />
      </div>
      <div className={styles.box} data-right>
        <label id="maxValueLabel" htmlFor="maxValue">
          Eind jaar
        </label>
        <input
          id="maxValue"
          type="number"
          min={totalRange[0]}
          max={totalRange[1]}
          value={numBoxValues.maxValue}
          onBlur={(e) => handleMaxNumBoxChange(e.target.value)}
          onChange={(e) => {
            const { value } = e.target
            // don't allow values larger than 4 chars
            if (value.length > 4) return

            // if value is exactly 4 chars, kick off validation
            if (value.length === 4) {
              handleMaxNumBoxChange(value)
              return
            }

            setNumBoxValues({
              ...numBoxValues,
              maxValue: value,
            })
          }}
        />
      </div>
    </div>
  )
}

export default SliderWithInputs
