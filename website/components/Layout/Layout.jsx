import Link from '../Link/Link'
import Header from '../Header/Header'
import Footer from '../Footer/Footer'

import styles from './Layout.module.css'

const Layout = ({ children }) => (
  <div className={styles.style}>
    <Link className={styles.skip} href="#main" external>
      Direct naar inhoud
    </Link>
    <Link className={styles.skip} href="#footer" external>
      Direct naar contactgegevens
    </Link>
    <Header title="Onderzoek en Statistiek" homeLink="/" />
    <main id="main">{children}</main>
    <Footer id="footer" />
  </div>
)

export default Layout
