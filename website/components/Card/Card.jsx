// Based on https://inclusive-components.design/cards/

import { forwardRef } from 'react'
import NextImage from 'next/image'

import videoIcon from '../../public/icons/video.svg'
import interactiveIcon from '../../public/icons/interactive.svg'
import { getStrapiMedia, PLACEHOLDER_IMAGE } from '../../lib/utils'
import { cx } from '../../lib/styleUtils'

import styles from './Card.module.css'

export const Card = forwardRef(
  ({ className, variant = 'small', children }, ref) => (
    <article
      ref={ref}
      className={cx(styles.style, className)}
      data-variant={variant}
    >
      {children}
    </article>
  ),
)

Card.displayName = 'Card'

export const CardHeadingGroup = ({ children, ...otherProps }) => (
  <hgroup className={styles.headingGroup} {...otherProps}>
    {children}
  </hgroup>
)

export const CardImage = ({
  type,
  fullWidth,
  image,
  priority,
  sizes,
  aspectRatio,
}) => {
  let icon
  if (type === 'video') {
    icon = videoIcon
  }
  if (type === 'interactive') {
    icon = interactiveIcon
  }

  return (
    <div
      className={styles.image}
      data-aspect-ratio={aspectRatio === '16-9' ? '16-9' : '4-3'}
      data-full-width={fullWidth || null}
    >
      <NextImage
        src={image ? getStrapiMedia(image) : PLACEHOLDER_IMAGE}
        alt=""
        priority={priority}
        fill
        sizes={sizes}
        style={{
          objectFit: 'cover',
        }}
      />
      {icon && (
        <div className={styles.icon}>
          <NextImage
            src={icon.src}
            alt=""
            width={48}
            height={48}
            style={{
              maxWidth: '100%',
              height: 'auto',
            }}
          />
        </div>
      )}
    </div>
  )
}
