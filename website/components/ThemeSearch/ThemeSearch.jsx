import { useState, useEffect, useContext } from 'react'
import debounce from 'lodash.debounce'

import { SearchContext, getSearchResults } from '../../lib/searchUtils'
import { trackSearchQuery } from '../../lib/analyticsUtils'
import CONTENT_TYPES from '../../constants/contentTypes'

import SearchBar from '../SearchBar/SearchBar'
import SearchResults from '../SearchResults/SearchResults'
import Link from '../Link/Link'

import styles from './ThemeSearch.module.css'

const NUMBER_OF_RESULTS = 5

const ThemeSearch = ({ themeTitle, slug }) => {
  const searchIndex = useContext(SearchContext)

  const [searchQuery, setSearchQuery] = useState('')
  const [category, setCategory] = useState('')
  const [results, setResults] = useState(
    getSearchResults(searchIndex, '', [slug], category),
  )

  useEffect(() => {
    const throttledUpdate = debounce(() => {
      const updatedResults = getSearchResults(
        searchIndex,
        searchQuery,
        [slug],
        category,
      )
      setResults(updatedResults)
    }, 300)
    throttledUpdate()
    return () => throttledUpdate.cancel()
  }, [slug, searchIndex, searchQuery, category])

  useEffect(() => {
    const hasQueryOrFilter = Boolean(searchQuery || category)

    if (hasQueryOrFilter) {
      const customDimensions = { dimension1: 'theme' }

      const tracker = trackSearchQuery(
        searchQuery || 'no query',
        category || 'no filter',
        undefined,
        customDimensions,
      )
      return () => tracker.cancel()
    }
    return undefined
  }, [searchQuery, category])

  useEffect(() => {
    setCategory('')
  }, [slug])

  return (
    <section className="mb-72 mb-lg-120" aria-label="Zoeken binnen dit thema">
      <h2 className="mb-16">{`Meer over ${themeTitle}`}</h2>
      <SearchBar
        id="searchTheme"
        type="text"
        ariaLabel="Doorzoek dit thema"
        value={searchQuery}
        onChange={(text) => setSearchQuery(text)}
      />
      <div
        className="mb-40 mb-lg-80 mt-8"
        role="group"
        aria-label="Filter zoekresultaten"
      >
        <button
          className={styles.button}
          type="button"
          onClick={() => setCategory('')}
          aria-pressed={category === ''}
          data-text="Alles"
        >
          Alles
        </button>
        {Object.values(CONTENT_TYPES)
          .filter((cat) => cat.type !== 'theme')
          .map(({ type, plural }) => (
            <button
              className={styles.button}
              key={plural}
              type="button"
              onClick={() => setCategory(type)}
              aria-pressed={category === type || null}
              data-text={plural}
            >
              {plural}
            </button>
          ))}
      </div>

      <SearchResults
        results={results?.slice(0, NUMBER_OF_RESULTS)}
        pageSize={NUMBER_OF_RESULTS}
        cardHeadingLevel="h3"
        extraMargin={false}
      />

      {results?.length === 0 && <p>Niets gevonden</p>}

      {results?.length > NUMBER_OF_RESULTS && (
        <Link variant="standalone" href={`/zoek?thema=${slug}`}>
          {`Alles over ${themeTitle}`}
        </Link>
      )}
    </section>
  )
}

export default ThemeSearch
