import slugify from 'slugify'

const extractMarkdownHeadings = (content, itemId) => {
  const headings = []

  // match the `#` syntax for headings
  const headingMatcher = /^(#+)\s(.+)$/gm

  let match = headingMatcher.exec(content)
  let counter = 0
  while (match !== null) {
    counter += 1
    const id = `${itemId}-${counter}`
    const level = match[1].length
    const title = match[2].trim()
    const slug = slugify(title, { lower: true, strict: true })
    headings.push({ id, slug, title, level })
    match = headingMatcher.exec(content)
  }
  return headings
}

export const getHeadings = (bodyContent) =>
  bodyContent.flatMap(({ type, text, id }) =>
    type === 'text' || type === 'text-with-links'
      ? extractMarkdownHeadings(text, id)
      : [],
  )
