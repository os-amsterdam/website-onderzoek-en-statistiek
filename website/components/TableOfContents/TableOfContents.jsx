import { useState, useRef } from 'react'

import useOnClickOutside from '../../lib/useOnClickOutside'
import { cx } from '../../lib/styleUtils'

import Icon from '../Icon/Icon'
import Link from '../Link/Link'

import { getHeadings } from './utils'
import styles from './TableOfContents.module.css'

const TableOfContents = ({ bodyContent }) => {
  const [open, setOpen] = useState(false)
  const headings = getHeadings(bodyContent)
  const ref = useRef()
  const closeMenu = () => setOpen(false)

  useOnClickOutside(ref, closeMenu)

  return (
    <div
      className={cx(
        styles.style,
        'col-start-1',
        'col-span-4',
        'col-span-lg-12',
      )}
    >
      <button
        className="mt-24"
        type="button"
        data-secondary
        data-small
        aria-label={open ? 'verberg inhoudsopgave' : 'toon inhoudsopgave'}
        aria-expanded={open}
        onClick={(e) => {
          setOpen(!open)
          e.stopPropagation()
        }}
      >
        Inhoud
        <Icon className="ml-12" size={14} name={open ? 'close' : 'list'} />
      </button>
      <nav ref={ref} data-is-open={open || null} aria-label="inhoudsopgave">
        <div>
          <ul>
            {headings.map(({ id, slug, title, level }) => {
              return (
                <li key={id} style={{ '--level': level }}>
                  <Link variant="inList" href={`#${slug}`} onClick={closeMenu}>
                    <Icon className="mr-12" size={14} />
                    {title}
                  </Link>
                </li>
              )
            })}
          </ul>
        </div>
      </nav>
    </div>
  )
}

export default TableOfContents
