import { useEffect, useRef, useState } from 'react'

import useOnMessage from '../../lib/useOnMessage'

import BodyBlock from '../BodyBlock/BodyBlock'
import SkeletonLoading from '../SkeletonLoading/SkeletonLoading'

import styles from './Embed.module.css'

const Embed = ({
  id,
  contentLink,
  title,
  variant,
  color,
  text,
  source,
  fixedHeight,
}) => {
  const [pageLoading, setPageLoading] = useState(true)
  const [frameLoading, setFrameLoading] = useState(true)
  const [height, setHeight] = useState(fixedHeight || 0)
  const ref = useRef()

  useEffect(() => {
    setPageLoading(false)
  }, [])

  function handlePostMessage(message) {
    if (message.type === 'frameHeightChanged' && message.id === id) {
      setHeight(message.payload)
    }
    return null
  }

  useOnMessage(handlePostMessage)

  useEffect(() => {
    // using height state to set loading state, because onLoad event on iframe
    // doesn't fire consistently. onLoad is still used to set loading state
    // when iframe source refuses connection
    if (height > 0) {
      setFrameLoading(false)
    }
  }, [height])

  // add id url param to contentLink
  const url = new URL(contentLink)
  url.searchParams.append('id', id)
  const urlWithId = url.toString()

  return (
    <BodyBlock title={title} variant={variant} color={color} text={text}>
      <div className={styles.style}>
        {
          // only render iframe when rest of the page has loaded,
          // to make sure page is ready for the iframes postMessage
          !pageLoading && (
            <iframe
              ref={ref}
              src={urlWithId}
              title={title}
              onLoad={() => setFrameLoading(false)}
              height={height}
              allow="clipboard-read; clipboard-write"
            />
          )
        }
        {frameLoading && <SkeletonLoading height={380} />}
      </div>
      {source && (
        <div className={styles.footer}>
          <p data-small>{`Bron: ${source}`}</p>
        </div>
      )}
    </BodyBlock>
  )
}

export default Embed
