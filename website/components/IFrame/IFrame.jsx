import { useEffect, useRef, useState } from 'react'

import useOnMessage from '../../lib/useOnMessage'

import SkeletonLoading from '../SkeletonLoading/SkeletonLoading'

import styles from './IFrame.module.css'

function updateUrlParams(payload) {
  const params = new URLSearchParams(payload)
  window.history.replaceState(
    {},
    '',
    `${window.location.pathname}?${params.toString()}`,
  )
}

const IFrame = ({ src, title }) => {
  const [pageLoading, setPageLoading] = useState(true)
  const [frameLoading, setFrameLoading] = useState(true)
  const [height, setHeight] = useState(0)
  const ref = useRef()

  useEffect(() => {
    setPageLoading(false)
  }, [])

  function handlePostMessage({ type, payload }) {
    switch (type) {
      case 'frameHeightChanged':
        setHeight(payload)
        break
      case 'frameStateChanged':
        updateUrlParams(payload)
        break
      default:
        return null
    }
    return null
  }

  useOnMessage(handlePostMessage)

  useEffect(() => {
    // using height state to set loading state, because onLoad event on iframe
    // doesn't fire consistently. onLoad is still used to set loading state
    // when iframe source refuses connection
    if (height > 0) {
      setFrameLoading(false)
    }
  }, [height])

  return (
    <div className={styles.style}>
      {
        // only render iframe when rest of the page has loaded,
        // to make sure page is ready for the iframes postMessage
        !pageLoading && (
          <iframe
            ref={ref}
            src={src}
            title={title}
            onLoad={() => setFrameLoading(false)}
            height={height}
            allow="clipboard-read; clipboard-write"
          />
        )
      }
      {frameLoading && <SkeletonLoading height={500} />}
    </div>
  )
}

export default IFrame
