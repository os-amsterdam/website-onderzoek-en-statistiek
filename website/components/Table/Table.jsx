import { useRef, useEffect } from 'react'

import { cx } from '../../lib/styleUtils'

import styles from './Table.module.css'

const Table = ({ className, children, ...otherProps }) => {
  const table = useRef()

  // add column title as custom attibute to each cell
  // so we can use it in css for small screens
  useEffect(() => {
    if (table.current) {
      const columnHeadings = table.current.getElementsByTagName('th')

      Array.from(table.current.getElementsByTagName('td')).forEach((cell) => {
        cell.setAttribute(
          'data-col',
          columnHeadings[cell.cellIndex].textContent,
        )
      })
    }
  }, [table])

  return (
    <table ref={table} className={cx(styles.style, className)} {...otherProps}>
      {children}
    </table>
  )
}

export default Table
