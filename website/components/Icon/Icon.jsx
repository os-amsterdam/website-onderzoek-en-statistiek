import { useEffect, useState } from 'react'

const Icon = ({ className, name, ...props }) => {
  // this whole wrapper for web compoennt os-icon is needed because of Nextjs hydration errors...
  const [isMounted, setIsMounted] = useState(false)
  useEffect(() => setIsMounted(true), [])
  return isMounted ? (
    <div data-name={name} style={{ display: 'contents' }}>
      <os-icon className={className} name={name} {...props} />
    </div>
  ) : (
    <div />
  )
}

export default Icon
