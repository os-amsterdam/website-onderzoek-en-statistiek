import { cx } from '../../lib/styleUtils'

import Icon from '../Icon/Icon'

import styles from './FallbackPage.module.css'

const FallbackPage = () => (
  <div className="grid">
    <div className={cx(styles.style, 'col-span-4', 'col-span-lg-12')}>
      <Icon name="spinner" size={40} />
    </div>
  </div>
)

export default FallbackPage
