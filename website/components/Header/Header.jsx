/* eslint-disable @next/next/no-img-element */
import { useEffect, useState, useRef } from 'react'
import { useRouter } from 'next/router'
import Link from 'next/link'

import { cx } from '../../lib/styleUtils'

import Navigation from '../Navigation/Navigation'
import Backdrop from '../Backdrop/Backdrop'

import styles from './Header.module.css'

const Header = ({ title, homeLink }) => {
  const [isOpen, setIsOpen] = useState(false)
  const [showTitle, setShowTitle] = useState(false)
  const router = useRouter()
  const observer = useRef()

  useEffect(() => {
    observer.current = new IntersectionObserver(
      // hide title when homePageTitle is on screen
      ([el]) => setShowTitle(!el.isIntersecting),
      { rootMargin: '-100px' },
    )
    return () => observer.current.disconnect()
  }, [])

  useEffect(() => {
    const homePageTitle = document.getElementById('homePageTitle')
    if (!homePageTitle) {
      // IntersectionObserver is a little slow detecting if the title is on screen,
      // which causes the title to flash. To fix this, title is hidden by default,
      // and only shown when showTitle is set to true here or by the IntersectionObserver
      setShowTitle(true)
      return
    }

    observer.current.observe(homePageTitle)
  }, [router.asPath])

  const withTitle = title && showTitle

  return (
    <>
      <header className={cx(styles.wrapper, 'grid')}>
        <h1
          className={cx(
            styles.heading,
            'row-start-1',
            'col-start-1',
            'col-span-4',
            'col-span-lg-8',
          )}
        >
          <Link
            href={homeLink}
            className={`${styles.link} analytics-sitelogo`}
            aria-label="Naar voorpagina website Onderzoek en Statistiek"
            data-with-title={withTitle || null}
          >
            <div className={styles.logo} data-with-title={withTitle || null}>
              <img
                className={styles.large}
                src="/logo-gemeente-amsterdam-large.svg"
                alt=""
              />
              <img
                className={styles.small}
                src="/logo-gemeente-amsterdam-small.svg"
                alt=""
              />
            </div>
            {title && (
              <div className={styles.title} data-with-title={withTitle || null}>
                {title}
              </div>
            )}
          </Link>
        </h1>

        <Navigation
          className={cx(
            styles.nav,
            'col-start-4',
            'row-start-1',
            'col-span-lg-4',
          )}
          isOpen={isOpen}
          setIsOpen={setIsOpen}
        />
      </header>
      <Backdrop isOpen={isOpen} onClick={() => setIsOpen(false)} />
    </>
  )
}

export default Header
