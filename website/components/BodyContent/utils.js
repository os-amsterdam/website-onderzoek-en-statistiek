export const getTextWidth = (
  variant,
  colStartText,
  colRangeText,
  colStartTextWithLink,
  colRangeTextWithLink,
) => {
  switch (variant) {
    case 'links':
      return {
        colStart: { small: 1, large: 5 },
        colRange: colRangeTextWithLink,
      }
    case 'onder':
      return {
        colStart: colStartText,
        colRange: colRangeText,
      }
    case 'rechts':
    default:
      return {
        colStart: colStartTextWithLink,
        colRange: colRangeTextWithLink,
      }
  }
}

export const getLinksWidth = (variant, colStartText, colRangeText) => {
  switch (variant) {
    case 'links':
      return {
        colStart: { small: 1, large: 1 },
        colRange: { small: 4, large: 3 },
      }
    case 'onder':
      return {
        colStart: colStartText,
        colRange: colRangeText,
      }
    case 'rechts':
    default:
      return {
        colStart: { small: 1, large: 10 },
        colRange: { small: 4, large: 3 },
      }
  }
}
