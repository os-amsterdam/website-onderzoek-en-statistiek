import { Fragment } from 'react'
import dynamic from 'next/dynamic'

import { normalizeBody } from '../../lib/normalizeUtils'

import GridItem from '../GridItem/GridItem'
import MarkdownToHtml from '../MarkdownToHtml/MarkdownToHtml'
import Link from '../Link/Link'
import Icon from '../Icon/Icon'

import { getTextWidth, getLinksWidth } from './utils'
import styles from './BodyContent.module.css'

const Embed = dynamic(() => import('../Embed/Embed'))
const TableOfContents = dynamic(
  () => import('../TableOfContents/TableOfContents'),
)
const Visualisation = dynamic(() => import('../Visualisation/Visualisation'), {
  ssr: false,
})
const VideoPlayer = dynamic(() => import('../VideoPlayer/VideoPlayer'))

const BodyContent = ({
  content,
  colStartText = { small: 1, large: 3 },
  colRangeText = { small: 4, large: 8 },
  colStartTextWithLink = { small: 1, large: 3 },
  colRangeTextWithLink = { small: 4, large: 6 },
  toc,
}) => (
  <>
    {toc && <TableOfContents bodyContent={normalizeBody(content)} />}
    {normalizeBody(content).map((item) => {
      if (item.type === 'text') {
        return (
          <GridItem
            key={item.id}
            colStart={colStartText}
            colRange={colRangeText}
          >
            <MarkdownToHtml>{item.text}</MarkdownToHtml>
          </GridItem>
        )
      }
      if (item.type === 'text-with-links') {
        return (
          <Fragment key={item.id}>
            <GridItem
              {...getTextWidth(
                item.variant,
                colStartText,
                colRangeText,
                colStartTextWithLink,
                colRangeTextWithLink,
              )}
            >
              <MarkdownToHtml toc={toc}>{item.text}</MarkdownToHtml>
            </GridItem>
            <GridItem
              as="aside"
              {...getLinksWidth(item.variant, colStartText, colRangeText)}
            >
              {item.links.length > 0 && (
                <>
                  {
                    // if there is no heading or an empty heading, we show a default text 'Zie ook'
                    // if editors don't want a heading, they can enter a single space in the cms
                    item.heading !== ' ' && (
                      <h2 data-style-as="h5" className="mb-12">
                        {item.heading || 'Zie ook'}
                      </h2>
                    )
                  }
                  <ul className="mb-24">
                    {item.links.map(
                      ({ name, path, title: linkTitle, shortTitle }) => (
                        <li key={path}>
                          <Link
                            href={path}
                            variant="inList"
                            external={name === 'link'}
                          >
                            <Icon
                              className="mr-12"
                              size={14}
                              name={
                                name === 'link'
                                  ? 'external-link'
                                  : 'chevron-right'
                              }
                            />

                            <span>
                              {name === 'link'
                                ? linkTitle
                                : `${shortTitle || linkTitle} - ${name}`}
                            </span>
                          </Link>
                        </li>
                      ),
                    )}
                  </ul>
                </>
              )}
            </GridItem>
          </Fragment>
        )
      }
      if (item.type === 'text-box') {
        return (
          <GridItem
            key={item.id}
            colStart={colStartText}
            colRange={colRangeText}
          >
            <aside className={styles.box}>
              <MarkdownToHtml>{item.text}</MarkdownToHtml>
            </aside>
          </GridItem>
        )
      }
      if (item.type === 'visualisation' || item.type === 'panel-group') {
        return <Visualisation key={item.id} {...item} />
      }
      if (item.type === 'embed') {
        return <Embed key={item.id} {...item} />
      }
      if (item.type === 'body-video') {
        return <VideoPlayer key={item.id} {...item} />
      }
      return null
    })}
  </>
)

export default BodyContent
