import { getStrapiMedia } from '../../lib/utils'

import BodyBlock from '../BodyBlock/BodyBlock'
import VideoButtons from '../VideoButtons/VideoButtons'

import styles from './VideoPlayer.module.css'

export const LocalVideo = ({
  videoSource,
  subtitleSource,
  enableSubtitleByDefault,
  poster,
}) => {
  const videoSourceStrapi = getStrapiMedia(videoSource)
  const subtitleSourceStrapi = getStrapiMedia(subtitleSource)

  return (
    <video
      className={styles.style}
      crossOrigin="anonymous"
      preload="metadata"
      muted={false}
      controls
      poster={getStrapiMedia(poster)}
    >
      <source src={videoSourceStrapi} type={videoSource.mime} />
      {subtitleSource && (
        <track
          default={!!enableSubtitleByDefault}
          src={subtitleSourceStrapi}
          kind="subtitles"
          srcLang="nl"
          label="Dutch"
        />
      )}
    </video>
  )
}

export const ExternalVideo = ({ source }) => (
  <div className={styles.external}>
    <iframe
      title="video-embed"
      src={source}
      frameBorder="0"
      allow="autoplay; fullscreen; picture-in-picture"
      allowFullScreen
    />
  </div>
)

const VideoPlayer = ({
  title,
  variant,
  color,
  text,
  videoFile,
  subtitleFile,
  transcript,
  subtitleDefault,
  externalVideoSource,
  poster,
}) => {
  return (
    <BodyBlock
      as="div"
      title={title}
      variant={variant}
      color={color}
      text={text}
    >
      {videoFile?.url ? (
        <LocalVideo
          videoSource={videoFile}
          subtitleSource={subtitleFile}
          enableSubtitleByDefault={subtitleDefault}
          poster={poster}
        />
      ) : (
        externalVideoSource && <ExternalVideo source={externalVideoSource} />
      )}
      <VideoButtons transcript={transcript} file={videoFile} />
    </BodyBlock>
  )
}

export default VideoPlayer
