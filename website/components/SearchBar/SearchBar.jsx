import { useRef, useEffect } from 'react'

import Icon from '../Icon/Icon'

import styles from './SearchBar.module.css'

const SearchBar = ({
  onChange,
  value,
  id,
  autoFocus,
  ariaLabel = 'Doorzoek website',
  ...otherProps
}) => {
  const searchInput = useRef(null)

  const handleOnClear = () => {
    onChange('')
    if (searchInput && searchInput.current) {
      searchInput.current.focus()
    }
  }

  // focus and scroll to the search bar on load is defined here, instead of
  // using autoFocus in order to prevent smooth scrolling
  useEffect(() => {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: 'instant',
    })

    if (autoFocus && searchInput && searchInput.current) {
      searchInput.current.focus()
    }
  }, [])

  return (
    <div className={styles.style} {...otherProps}>
      <input
        id={id}
        type="search"
        aria-label={ariaLabel}
        value={value}
        onChange={(e) => onChange(e.target.value)}
        ref={searchInput}
        autoComplete="off"
      />
      <div>
        {value ? (
          <button onClick={handleOnClear} aria-label="Wis zoekterm">
            <Icon size={24} name="close" />
          </button>
        ) : (
          <Icon className={styles.search} size={24} name="search" />
        )}
      </div>
    </div>
  )
}

export default SearchBar
