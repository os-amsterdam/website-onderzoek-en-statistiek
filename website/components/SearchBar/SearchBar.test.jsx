import { render, screen, fireEvent } from '@testing-library/react'

import SearchBar from './SearchBar'

describe('SearchBar', () => {
  window.scrollTo = jest.fn()

  it('should render search box', () => {
    render(<SearchBar />)
    expect(screen.getByRole('searchbox')).toBeInTheDocument()
  })

  it('should call onChange function when input value changes', () => {
    const mockFn = jest.fn()
    render(<SearchBar onChange={mockFn} />)
    const input = screen.getByRole('searchbox')

    fireEvent.change(input, { target: { value: 'test' } })

    expect(mockFn).toHaveBeenCalledWith('test')
  })

  it('should not render the clear button initially', () => {
    render(<SearchBar />)

    const clearButton = screen.queryByRole('button')

    expect(clearButton).not.toBeInTheDocument()
  })

  it('should render the clear button when SearchBox has text', () => {
    render(<SearchBar value="text" />)

    expect(screen.getByRole('button')).toBeInTheDocument()
  })

  it('should call the onChange function with an empty string when the clear button is clicked', () => {
    const mockFn = jest.fn()
    render(<SearchBar value="test" onChange={mockFn} />)
    const input = screen.getByRole('searchbox')

    expect(input.value).toBe('test')

    const clearButton = screen.getByLabelText('Wis zoekterm')
    fireEvent.click(clearButton)

    expect(mockFn).toHaveBeenCalledWith('')
  })

  it('should focus on the search bar when autoFocus is true', () => {
    render(<SearchBar autoFocus />)
    const input = screen.getByRole('searchbox')

    expect(input).toHaveFocus()
  })
})
