import { forwardRef } from 'react'
import slugify from 'slugify'

import GridItem from '../GridItem/GridItem'
import MarkdownToHtml from '../MarkdownToHtml/MarkdownToHtml'

import { cx } from '../../lib/styleUtils'

const getBlockLayout = (variant) => {
  switch (variant) {
    case 'links':
      return {
        colStart: { small: 1, large: 2 },
        colRange: { small: 4, large: 6 },
      }
    case 'rechts':
      return {
        colStart: { small: 1, large: 6 },
        colRange: { small: 4, large: 6 },
      }
    default:
      return {
        colStart: { small: 1, large: 3 },
        colRange: { small: 4, large: 8 },
      }
  }
}

const getTextLayout = (variant) => {
  switch (variant) {
    case 'links':
      return {
        colStart: { small: 1, large: 8 },
        colRange: { small: 4, large: 4 },
      }
    case 'rechts':
      return {
        colStart: { small: 1, large: 2 },
        colRange: { small: 4, large: 4 },
      }
    default:
      return {
        colStart: { small: 1, large: 3 },
        colRange: { small: 4, large: 8 },
        rowStart: 3,
      }
  }
}

const BodyBlock = forwardRef(
  ({ as = 'section', title, variant, children, text }, ref) => {
    // we use this variable to determine if we need to add margin to the bottom
    // of the whole BodyBlock or between the visualisation and the text below
    const hasTextBelowCenteredBlock = text && variant === 'gecentreerd'

    return (
      <GridItem
        as={as}
        aria-label={title}
        colStart={{ small: 1, large: 1 }}
        colRange={{ small: 4, large: 12 }}
      >
        <div
          className={cx('grid', !hasTextBelowCenteredBlock && 'mb-40')}
          ref={ref}
        >
          <GridItem
            colStart={{
              small: 1,
              large: variant === 'links' || variant === 'rechts' ? 2 : 3,
            }}
            colRange={{ small: 4, large: 8 }}
          >
            {title && (
              <h2
                data-style-as="h5"
                className="mb-40"
                id={slugify(title, { lower: true, strict: true })}
              >
                {title}
              </h2>
            )}
          </GridItem>

          <GridItem {...getBlockLayout(variant)} rowStart={2}>
            {children}
          </GridItem>

          {text && (
            <GridItem {...getTextLayout(variant)}>
              <div className={hasTextBelowCenteredBlock ? 'mt-40' : ''}>
                <MarkdownToHtml>{text}</MarkdownToHtml>
              </div>
            </GridItem>
          )}
        </div>
      </GridItem>
    )
  },
)

BodyBlock.displayName = 'BodyBlock'

export default BodyBlock
