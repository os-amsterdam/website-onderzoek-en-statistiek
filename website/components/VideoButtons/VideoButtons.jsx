import { useState, useId } from 'react'

import { getStrapiMedia } from '../../lib/utils'

import DownloadButton from '../DownloadButton/DownloadButton'
import ToggleButton from '../ToggleButton/ToggleButton'
import MarkdownToHtml from '../MarkdownToHtml/MarkdownToHtml'

import styles from './VideoButtons.module.css'

const VideoButtons = ({ file, transcript }) => {
  const toggleButtonId = useId()
  const transcriptId = useId()

  const [transcriptOpen, setTranscriptOpen] = useState(false)

  return (
    <div className="mb-56 mb-lg-80">
      {(file || transcript) && (
        <div className={styles.style}>
          <div>
            {file?.url && (
              <DownloadButton
                url={getStrapiMedia(file)}
                variant="textButton"
                type="video"
                data-small
              >
                Download video
              </DownloadButton>
            )}
            {transcript && (
              <ToggleButton
                id={toggleButtonId}
                open={transcriptOpen}
                setOpen={setTranscriptOpen}
                controlsId={transcriptId}
                data-small
              >
                Uitgeschreven tekst
              </ToggleButton>
            )}
          </div>
          {transcript && (
            <div
              id={transcriptId}
              data-open={transcriptOpen || null}
              aria-labelledby={toggleButtonId}
            >
              <MarkdownToHtml>{transcript}</MarkdownToHtml>
            </div>
          )}
        </div>
      )}
    </div>
  )
}

export default VideoButtons
