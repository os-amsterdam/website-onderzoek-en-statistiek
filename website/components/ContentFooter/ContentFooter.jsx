import { useState, useEffect } from 'react'

import { cx } from '../../lib/styleUtils'
import { pushCustomEvent } from '../../lib/analyticsUtils'

import Link from '../Link/Link'
import Icon from '../Icon/Icon'

import styles from './ContentFooter.module.css'

const ContentFooter = ({ type, themes }) => {
  const [shareLinkObject, setShareLinkObject] = useState()

  useEffect(() => {
    setShareLinkObject({
      href: window.location.href,
      title: window.document.title,
    })
  }, [])

  return (
    <div
      className={cx(
        styles.container,
        'mt-40',
        'mb-72',
        'mt-lg-80',
        'mb-lg-120',
      )}
    >
      <div className="mb-12">
        {themes.length > 0 && (
          <>
            <p className={styles.text} data-small>
              {`${type === 'artikel' || type === 'dossier' ? 'Dit' : 'Deze'}
           ${type} maakt deel uit van
           ${themes.length > 1 ? 'de thema’s' : ' het thema'}
          `}
            </p>
            <ul className={styles.list} data-small>
              {themes.map(({ title, slug }) => (
                <li className={styles.listItem} key={slug}>
                  <Link
                    data-small
                    href={`/thema/${slug}`}
                    className="analytics-contentpage-theme-link"
                  >
                    {title}
                  </Link>
                </li>
              ))}
            </ul>
          </>
        )}
      </div>
      <div className={styles.share}>
        <p data-small>Deel deze pagina:</p>
        <ul className={styles.buttons}>
          <li>
            <button
              type="button"
              title="Deel op Facebook"
              aria-label="Deel op Facebook"
              onClick={() => {
                pushCustomEvent(
                  'Share',
                  'Facebook',
                  shareLinkObject.href.split('/').pop(),
                )
                window.open(
                  `https://www.facebook.com/sharer/sharer.php?u=${shareLinkObject.href}`,
                  '_blank',
                )
              }}
            >
              <Icon name="facebook" size={18} />
            </button>
          </li>
          <li>
            <button
              type="button"
              title="Deel op LinkedIn"
              aria-label="Deel op LinkedIn"
              onClick={() => {
                pushCustomEvent(
                  'Share',
                  'LinkedIn',
                  shareLinkObject.href.split('/').pop(),
                )
                window.open(
                  `https://www.linkedin.com/sharing/share-offsite/?url=${shareLinkObject.href}`,
                  '_blank',
                )
              }}
            >
              <Icon name="linkedin" size={18} />
            </button>
          </li>
          <li>
            <button
              type="button"
              title="Deel via mail"
              aria-label="Deel via mail"
              onClick={() => {
                pushCustomEvent(
                  'Share',
                  'Mail',
                  shareLinkObject.href.split('/').pop(),
                )
                window.open(
                  `mailto:?subject=${shareLinkObject.title}&body=Zie: ${escape(
                    shareLinkObject.href,
                  )}`,
                  '_self',
                )
              }}
            >
              <Icon name="email" size={18} />
            </button>
          </li>
          <li>
            <button
              type="button"
              title="Print deze pagina"
              aria-label="Print deze pagina"
              onClick={() => {
                pushCustomEvent(
                  'Share',
                  'Print',
                  shareLinkObject.href.split('/').pop(),
                )
                window.print()
              }}
            >
              <Icon size={18} name="print" />
            </button>
          </li>
        </ul>
      </div>
    </div>
  )
}

export default ContentFooter
