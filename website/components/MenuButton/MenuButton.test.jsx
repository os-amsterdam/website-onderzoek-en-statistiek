import { render, screen } from '@testing-library/react'

import MenuButton from './MenuButton'

describe('MenuButton', () => {
  it('should render without crashing', () => {
    render(<MenuButton />)
    expect(screen.getByRole('button')).toBeInTheDocument()
  })
})
