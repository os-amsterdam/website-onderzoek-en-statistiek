import { forwardRef } from 'react'

import { cx } from '../../lib/styleUtils'

import styles from './MenuButton.module.css'

const MenuButton = forwardRef(
  ({ children, className, isOpen, setIsOpen, ...otherProps }, ref) => (
    <button
      className={cx(styles.button, className)}
      data-small
      data-is-open={isOpen || null}
      onClick={() => setIsOpen(!isOpen)}
      {...otherProps}
      ref={ref}
    >
      <span className={styles.label}>{children}</span>
      <span className={styles.icon} data-is-open={isOpen || null} />
    </button>
  ),
)

MenuButton.displayName = 'MenuButton'

export default MenuButton
