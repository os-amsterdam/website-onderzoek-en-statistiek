import { cx } from '../../lib/styleUtils'

import styles from './ByLine.module.css'

const ByLine = ({ className, items }) => {
  return (
    <ul className={cx(styles.style, className)} aria-label="Metadata">
      {items
        .filter((item) => item)
        .map((item) => (
          <li key={item}>{item}</li>
        ))}
    </ul>
  )
}

export default ByLine
