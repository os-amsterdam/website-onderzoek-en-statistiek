import { dateConfig, formatDate } from '../../lib/utils'
import { cx } from '../../lib/styleUtils'
import CONTENT_TYPES from '../../constants/contentTypes'

import Link from '../Link/Link'
import { Card, CardHeadingGroup } from '../Card/Card'
import SkeletonLoading from '../SkeletonLoading/SkeletonLoading'

const SearchResults = ({
  results,
  page = 1,
  pageSize = 10,
  firstResultRef,
  extraMargin = true,
}) => (
  <ul>
    {results
      ? results
          .slice((page - 1) * pageSize, page * pageSize)
          .map(
            (
              {
                slug,
                title,
                teaser,
                type,
                publicationDate,
                formatPublicationDate,
              },
              idx,
            ) => {
              const date = formatDate(
                publicationDate,
                dateConfig(formatPublicationDate),
              )

              return (
                <li key={`${type}-${slug}`}>
                  <Card
                    data-variant="small"
                    className={cx('mb-40', extraMargin && 'mb-lg-80')}
                    ref={idx === 0 ? firstResultRef : null}
                  >
                    <CardHeadingGroup>
                      <h3 data-style-as="h5">
                        <Link
                          data-inline
                          href={`/${CONTENT_TYPES[type].name}/${slug}`}
                        >
                          {title}
                        </Link>
                      </h3>
                      <p data-small>{CONTENT_TYPES[type.toLowerCase()].name}</p>
                    </CardHeadingGroup>
                    <p className="mb-8">{teaser}</p>
                    <p data-small>
                      {type === 'collection' ||
                      type === 'dataset' ||
                      type === 'interactive'
                        ? `Laatst gewijzigd: ${date}`
                        : `Gepubliceerd: ${date}`}
                    </p>
                  </Card>
                </li>
              )
            },
          )
      : [...Array(pageSize).keys()].map((index) => (
          <SkeletonLoading
            className={cx('mb-40', extraMargin && 'mb-lg-80')}
            height={180}
            key={index}
          />
        ))}
  </ul>
)

export default SearchResults
