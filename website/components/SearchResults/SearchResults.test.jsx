import { render, screen } from '@testing-library/react'

import SearchResults from './SearchResults'

describe('SearchResults component', () => {
  const results = [
    { slug: 'test-result-1', type: 'article' },
    { slug: 'test-result-2', type: 'article' },
    { slug: 'test-result-3', type: 'publication' },
  ]

  it('renders the search results list', () => {
    render(<SearchResults results={results} />)
    const list = screen.getByRole('list')
    expect(list).toBeInTheDocument()
  })

  it('renders the correct number of search results', () => {
    render(<SearchResults results={results} />)
    const items = screen.getAllByRole('listitem')
    expect(items.length).toBe(3)
  })

  it('renders the skeleton loading when there are no search results', () => {
    render(<SearchResults pageSize={3} />)
    const skeletonLoadings = screen.getAllByTestId('skeleton-loading')
    expect(skeletonLoadings.length).toBe(3)
  })
})
