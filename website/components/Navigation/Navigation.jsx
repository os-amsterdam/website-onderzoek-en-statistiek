import { useEffect, useRef } from 'react'
import { useRouter } from 'next/router'

import { cx } from '../../lib/styleUtils'

import MenuButton from '../MenuButton/MenuButton'
import MegaMenu from '../MegaMenu/MegaMenu'
import Link from '../Link/Link'

import styles from './Navigation.module.css'

const Navigation = ({ className, isOpen, setIsOpen }) => {
  const MenuButtonRef = useRef()
  const router = useRouter()

  // close the menu when navigating to a different page
  useEffect(() => {
    if (isOpen) {
      setIsOpen(false)
    }
  }, [router.asPath])

  // close the menu and focus on MenuButton on 'Escape' button press
  const closeAndFocusOnEscape = (e) => {
    if (e.key === 'Escape') {
      setIsOpen(false)
      return isOpen && MenuButtonRef.current.focus()
    }
    return null
  }

  return (
    <nav className={cx(styles.style, className)} aria-label="Hoofdmenu">
      <ul
        onKeyDown={(e) => {
          closeAndFocusOnEscape(e)
        }}
        onBlur={(e) => {
          if (!e.currentTarget.contains(e.relatedTarget)) {
            setIsOpen(false)
          }
        }}
      >
        <li>
          <Link
            className={styles.link}
            variant="inList"
            href="/zoek"
            aria-current={router.asPath === '/zoek' && 'page'}
          >
            Zoeken
          </Link>
        </li>
        <li>
          <MenuButton
            type="button"
            isOpen={isOpen}
            setIsOpen={setIsOpen}
            aria-expanded={isOpen}
            aria-controls="menu"
            aria-label="menu"
            ref={MenuButtonRef}
            data-small
          >
            Menu
          </MenuButton>
          <MegaMenu isOpen={isOpen} currentPath={router.asPath} />
        </li>
      </ul>
    </nav>
  )
}

export default Navigation
