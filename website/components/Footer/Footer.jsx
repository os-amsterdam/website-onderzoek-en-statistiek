import { cx } from '../../lib/styleUtils'

import Icon from '../Icon/Icon'
import Link from '../Link/Link'

import styles from './Footer.module.css'

const Footer = (props) => (
  <footer aria-labelledby="headingFooter" {...props}>
    <h1 id="headingFooter" className="hide-visually">
      Colofon
    </h1>
    <div className={cx(styles.container, 'grid')}>
      <div className="col-span-3">
        <h2 className="mb-28" data-style-as="h5" data-inverted>
          Contact
        </h2>
        <p className="mb-16" data-small data-inverted>
          Heeft u een vraag en kunt u het antwoord niet vinden op deze site?
          Neem dan contact met ons op.
        </p>
        <ul className="mb-40 mb-lg-0">
          <li>
            <Link
              data-inverted
              href="mailto:redactie.os@amsterdam.nl"
              external
              data-variant="inList"
            >
              <Icon size={20} name="email" />
              E-mail
            </Link>
          </li>
          <li>
            <Link
              data-inverted
              href="tel:+31202510333"
              external
              data-variant="inList"
            >
              <Icon size={20} name="phone" />
              020 251 0333
            </Link>
          </li>
        </ul>
      </div>
      <div className="col-span-3 col-start-1 col-start-lg-5">
        <h2 className="mb-28" data-style-as="h5" data-inverted>
          Panels en enquêtes
        </h2>
        <p className="mb-16" data-small data-inverted>
          Bent u uitgenodigd om mee te doen aan onderzoek of heeft u vragen over
          het panel of stadspaspanel?
        </p>
        <ul className="mb-40 mb-lg-0">
          <li>
            <Link
              data-inverted
              href="https://www.amsterdam.nl/onderzoek/"
              data-variant="inList"
              external
            >
              <Icon size={14} name="chevron-right" />
              Meedoen aan onderzoek
            </Link>
          </li>
          <li>
            <Link
              data-inverted
              href="https://panel.amsterdam.nl"
              variant="inList"
              external
            >
              <Icon size={14} name="chevron-right" />
              Panel Amsterdam
            </Link>
          </li>
          <li>
            <Link
              data-inverted
              href="https://panel.amsterdam.nl/stadspas/home.html"
              data-variant="inList"
              external
            >
              <Icon size={14} name="chevron-right" />
              Stadspaspanel Amsterdam
            </Link>
          </li>
        </ul>
      </div>
      <div className="col-span-3 col-span-lg-4 col-start-1 col-start-lg-9">
        <h2 data-style-as="h5" data-inverted>
          Onderzoek en Statistiek
        </h2>
        <ul className="mb-40 mb-lg-0">
          <li>
            <Link
              href="/artikel/over-onderzoek-en-statistiek"
              data-inverted
              data-variant="inList"
            >
              <Icon size={14} name="chevron-right" />
              Over Onderzoek en Statistiek
            </Link>
          </li>
          <li>
            <Link
              href="/artikel/veelgestelde-vragen"
              data-inverted
              data-variant="inList"
            >
              <Icon size={14} name="chevron-right" />
              Veelgestelde vragen
            </Link>
          </li>
          <li>
            <Link
              href="/artikel/termen-en-categorieen"
              data-inverted
              data-variant="inList"
            >
              <Icon size={14} name="chevron-right" />
              Termen en categorieën
            </Link>
          </li>
          <li>
            <Link
              href="https://nieuwsbrieven.amsterdam.nl/x/plugin/?pName=subscribe&MIDRID=S7Y1NAAAA04&pLang=nl&Z=-1181930969"
              variant="inList"
              data-inverted
              external
            >
              <Icon size={14} name="chevron-right" />
              Nieuwsbrief
            </Link>
          </li>
          <li>
            <Link
              href="https://www.amsterdam.nl/bestuur-organisatie/werkenbij/externe/?zoeken=true&zoeken_term=&selectie_zoeken_op_maanden=AllYears&zoek_clustered_keywords_cluster=3669_3670_3671_3672_3673_3674_3675_3676_3677_3678_3679_3717_3681_3682_3683_3684_3685_3686_3688&zoek_clustered_keywords=3717&zoek_clustered_keywords_cluster=3662_3664_3665_3666_3667_3668_3715&zoek_clustered_keywords_cluster=3689_3690_3692_3693"
              data-variant="inList"
              data-inverted
              external
            >
              <Icon size={14} name="chevron-right" />
              Vacatures
            </Link>
          </li>
        </ul>
      </div>
    </div>
    <div className={styles.bottom}>
      <h2 className="hide-visually">Privacy en toegankelijkheid</h2>
      <ul>
        <li>
          <Link
            href="https://www.amsterdam.nl/privacy/"
            data-variant="inList"
            external
          >
            Privacy
          </Link>
        </li>
        <li>
          <Link
            href="/artikel/toegankelijkheidsverklaring"
            data-variant="inList"
          >
            Toegankelijkheid
          </Link>
        </li>
      </ul>
    </div>
  </footer>
)

export default Footer
