import { useState, useEffect, useRef } from 'react'

import useIsMobile from '../../lib/useIsMobile'
import useTrappedFocus from '../../lib/useTrappedFocus'
import { calculateFacetsTotals, formatFacetNumber } from '../../lib/searchUtils'
import CONTENT_TYPES from '../../constants/contentTypes'
import { cx } from '../../lib/styleUtils'

import SliderWithInputs from '../SliderWithInputs/SliderWithInputs'
import Backdrop from '../Backdrop/Backdrop'
import Icon from '../Icon/Icon'
import SkeletonLoading from '../SkeletonLoading/SkeletonLoading'

import styles from './SearchFilters.module.css'

const SearchFilters = ({
  results,
  themes,
  themeFilter,
  handleThemeChange,
  category,
  setCategory,
  setPage,
  period,
  setPeriod,
  periodRange,
}) => {
  const [facetCount, setfacetCount] = useState(null)
  const [mobilePanelOpen, setMobilePanelOpen] = useState(false)

  const mobilePanelRef = useRef(null)
  const { keyDown } = useTrappedFocus(mobilePanelRef)
  const isMobile = useIsMobile()

  useEffect(() => {
    if (results) {
      setfacetCount(calculateFacetsTotals(themes, CONTENT_TYPES, results))
    }
  }, [results, themes])

  useEffect(() => {
    if (mobilePanelOpen) {
      // block background scroll when mobile panel is open
      document.body.style.overflow = 'hidden'
      // scroll to top when mobile panel opens
      mobilePanelRef.current.scrollTop = 0
    } else {
      document.body.style.overflow = 'unset'
    }
  }, [mobilePanelOpen])

  useEffect(() => {
    if (!isMobile) {
      // close mobile panel on resize, if bigger than breakpoint
      setMobilePanelOpen(false)
    }
  }, [isMobile])

  useEffect(() => {
    if (!mobilePanelOpen || !mobilePanelRef.current) {
      return
    }

    const firstFocusableEl = mobilePanelRef.current.querySelector('button')

    if (firstFocusableEl instanceof HTMLElement) {
      // focus on first button in mobile panel
      firstFocusableEl.focus()
    }
  }, [mobilePanelOpen])

  return (
    <>
      <section
        className={cx(
          styles.container,
          'row-start-3',
          'row-start-lg-2',
          'col-span-4',
        )}
        aria-labelledby="headingSearchFilters"
        data-is-open={mobilePanelOpen || null}
        ref={mobilePanelRef}
        onKeyDown={keyDown}
      >
        <h2 id="headingSearchFilters" className="hide-visually">
          Filters
        </h2>
        <div className={styles.mobileHeader}>
          <h2 data-style-as="h3">Filters</h2>
          <button
            type="button"
            aria-label="Sluit filtersectie"
            onClick={() => setMobilePanelOpen(false)}
          >
            <Icon size={24} name="close" />
          </button>
        </div>
        <fieldset>
          <legend>
            <h3 className="mb-24" data-style-as="h5">
              Thema’s
            </h3>
          </legend>
          {themes
            .slice() // strict mode freezes arrays, so we need to make a copy to be able to sort
            .sort((a, b) => a.title.localeCompare(b.title))
            .map(({ title, slug }) => (
              <label className="mb-16" data-small key={slug}>
                <input
                  type="checkbox"
                  aria-label={`${title} ${
                    facetCount ? formatFacetNumber(facetCount[slug]) : ''
                  } items gevonden.`}
                  onChange={() => handleThemeChange(slug)}
                  checked={themeFilter.includes(slug)}
                />
                {`${title} ${
                  facetCount ? formatFacetNumber(facetCount[slug]) : ''
                }`}
              </label>
            ))}
        </fieldset>

        <fieldset>
          <legend>
            <h3 className="mb-24" data-style-as="h5">
              Periode
            </h3>
          </legend>
          {periodRange[0] && periodRange[1] ? (
            <SliderWithInputs
              selectedRange={period}
              totalRange={periodRange}
              onChange={({ minValue, maxValue }) => {
                setPeriod([minValue, maxValue])
                setPage(1)
              }}
            />
          ) : (
            <SkeletonLoading height={120} />
          )}
        </fieldset>
        <fieldset>
          <legend>
            <h3 className="mb-24" data-style-as="h5">
              Categorieën
            </h3>
          </legend>
          <label className="mb-16" data-small>
            <input
              type="radio"
              name="categories"
              id="alles"
              onChange={() => {
                setCategory('')
                setPage(1)
              }}
              checked={category === ''}
            />
            Alle categorieën
          </label>
          {Object.values(CONTENT_TYPES)
            .filter((cat) => cat.type !== 'theme')
            .map(({ type, plural }) => (
              <label className="mb-16" key={type} data-small>
                <input
                  type="radio"
                  name="categories"
                  id={type}
                  onChange={() => {
                    setCategory(type)
                    setPage(1)
                  }}
                  checked={category === type}
                />
                {`${plural} ${
                  facetCount ? formatFacetNumber(facetCount[type]) : ''
                }`}
              </label>
            ))}
        </fieldset>
        <div className={styles.mobileFooter}>
          {results && (
            <button
              data-primary
              type="button"
              onClick={() => setMobilePanelOpen(false)}
            >
              {`Toon ${results.length} ${
                results.length === 1 ? 'resultaat' : 'resultaten'
              }`}
            </button>
          )}
        </div>
      </section>

      <button
        className={styles.toggle}
        data-primary
        type="button"
        onClick={() => {
          window.scrollTo({
            top: 0,
            left: 0,
            behavior: 'instant',
          })
          return setMobilePanelOpen(true)
        }}
      >
        Filter
      </button>

      <Backdrop
        isOpen={mobilePanelOpen}
        zIndex={502}
        onClick={() => setMobilePanelOpen(false)}
      />
    </>
  )
}

export default SearchFilters
