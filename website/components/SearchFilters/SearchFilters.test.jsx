import React from 'react'
import { render, fireEvent } from '@testing-library/react'

import SearchFilters from './SearchFilters'

jest.mock('../../lib/useIsMobile', () => {
  return () => false
})

// TODO: test mobile functions, open and close filter section buttons

describe('SearchFilters component', () => {
  const results = [
    { slug: 'test-result-1', type: 'article', theme: ['theme-1'] },
    { slug: 'test-result-2', type: 'article', theme: ['theme-1'] },
    { slug: 'test-result-3', type: 'publication', theme: ['theme-1'] },
  ]
  const themes = [
    { title: 'Theme 1', slug: 'theme-1' },
    { title: 'Theme 2', slug: 'theme-2' },
  ]
  const themeFilter = ['theme-1']
  const category = 'publication'

  const period = ['2022', '2023']
  const periodRange = ['2000', '2023']

  const handleThemeChange = jest.fn()
  const setCategory = jest.fn()
  const setPage = jest.fn()
  const setPeriod = jest.fn()

  // because the component needs a lot of props to be initialized,
  // I've bundled several related tests in a single test here
  it('calls a function when the corresponding control is changed', () => {
    const { getByLabelText, getAllByRole } = render(
      <SearchFilters
        results={results}
        themes={themes}
        themeFilter={themeFilter}
        period={period}
        periodRange={periodRange}
        category={category}
        handleThemeChange={handleThemeChange}
        setCategory={setCategory}
        setPage={setPage}
        setPeriod={setPeriod}
      />,
    )

    // it calls the handleThemeChange function when a theme checkbox is clicked
    const themeCheckbox = getByLabelText('Theme 1 (3)')
    fireEvent.click(themeCheckbox)
    expect(handleThemeChange).toHaveBeenCalledWith('theme-1')

    // it calls the setCategory function when a category radio button is clicked
    const categoryRadio = getByLabelText('Artikelen (2)')
    fireEvent.click(categoryRadio)
    expect(setCategory).toHaveBeenCalledWith('article')

    // it calls the setPeriod and setPage functions when the period slider is changed
    const periodSlider = getAllByRole('slider')
    fireEvent.change(periodSlider[0], { target: { value: 2000 } })
    fireEvent.change(periodSlider[1], { target: { value: 2020 } })
    expect(setPeriod).toHaveBeenCalledWith(['2000', '2020'])
    expect(setPage).toHaveBeenCalledWith(1)
  })
})
