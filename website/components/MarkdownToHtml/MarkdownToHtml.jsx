import ReactMarkdownComponent from 'react-markdown'
import remarkGfm from 'remark-gfm'
import slugify from 'slugify'

import InlineImage from '../InlineImage/InlineImage'
import Link from '../Link/Link'
import Table from '../Table/Table'

import styles from './MarkdownToHtml.module.css'

const createId = (title) =>
  title && slugify(title.toString(), { lower: true, strict: true })

const markdownToHtmlMap = {
  h1: ({ children }) => (
    <h2 className="mb-40" data-style-as="h3" id={createId(children)}>
      {children}
    </h2>
  ),
  h2: ({ children }) => (
    <h2 className="mb-40" data-style-as="h3" id={createId(children)}>
      {children}
    </h2>
  ),
  h3: ({ children }) => (
    <h2 className="mb-40" data-style-as="h4" id={createId(children)}>
      {children}
    </h2>
  ),
  h4: ({ children }) => (
    <h2 className="mb-40" data-style-as="h5" id={createId(children)}>
      {children}
    </h2>
  ),
  h5: ({ children }) => (
    <h2 className="mb-40" data-style-as="h5" id={createId(children)}>
      {children}
    </h2>
  ),
  p: ({ children }) => {
    // this check identifies standalone links and renders them with the correct styling
    if (children[0]?.type?.name === 'a' && children.length === 1) {
      return <Link external data-variant="standalone" {...children[0].props} />
    }

    // a paragraph starting with ^ is rendered with a smaller font size
    // with this small hack we don't need to use a plugin that allows all html in markdown
    if (
      children[0] &&
      typeof children[0] === 'string' &&
      children[0].startsWith('^')
    ) {
      const newChildren =
        typeof children === 'string'
          ? children.replace('^', '')
          : [children[0].replace('^', ''), ...children.slice(1)]

      return (
        <p className="mb-40" data-small>
          {newChildren}
        </p>
      )
    }

    return <p className="mb-40">{children}</p>
  },
  blockquote: ({ node }) => (
    // this hacky code is necessary because Strapi's default rich text editor always
    // wraps text in a blockquote in a paragraph, which gets rendered with Paragraph styling.
    // This takes the text from the paragraph and puts it directly in the blockquote.
    <blockquote className="mb-40">
      {node.children[1]?.children[0]?.value}
    </blockquote>
  ),
  img: (props) => <InlineImage {...props} />,
  a: ({ href, ...otherProps }) => (
    <Link
      href={href}
      external={href.startsWith('http')}
      // Only links to external websites open in a new tab / window
      target={href.startsWith('http') ? '_blank' : undefined}
      data-inline
      {...otherProps}
    />
  ),
  ul: ({ children }) => (
    <ul className="mb-40" data-ul>
      {children}
    </ul>
  ),
  ol: ({ children }) => (
    <ol className="mb-40" data-ol>
      {children}
    </ol>
  ),
  table: ({ children }) => <Table className="mb-40">{children}</Table>,
}

const MarkdownToHtml = ({ children }) => (
  <ReactMarkdownComponent
    className={styles.style}
    remarkPlugins={[remarkGfm]}
    components={markdownToHtmlMap}
  >
    {children}
  </ReactMarkdownComponent>
)

export default MarkdownToHtml
