import Icon from '../Icon/Icon'

import styles from './ToggleButton.module.css'

const ToggleButton = ({
  className,
  open,
  setOpen,
  controlsId,
  children,
  ...otherProps
}) => (
  <button
    className={className}
    type="button"
    data-small
    aria-controls={controlsId}
    aria-expanded={open}
    onClick={() => setOpen(!open)}
    {...otherProps}
  >
    <Icon class={styles.icon} data-is-open={open || null} size={20} />

    {children}
  </button>
)

export default ToggleButton
