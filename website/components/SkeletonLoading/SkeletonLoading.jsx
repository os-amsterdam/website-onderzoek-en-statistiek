import { cx, propsToCssVars } from '../../lib/styleUtils'

import styles from './SkeletonLoading.module.css'

const SkeletonLoading = ({ className, height, width }) => {
  const style = propsToCssVars([{ width }, { height }])

  return (
    <div
      className={cx(styles.style, className)}
      style={style}
      data-testid="skeleton-loading"
    />
  )
}

export default SkeletonLoading
