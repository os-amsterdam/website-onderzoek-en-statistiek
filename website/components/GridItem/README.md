# GridItem component

The following code would place the component on the second row and third column (of 12).

`<GridItem colStart={3} rowStart={2}>`

With the `colRange` and `rowRange` props you define how many columns and rows the component spans.
The following code would have the component span two rows and four columns.

`<GridItem colRange={4} rowRange={2}>`

You can set all four of these props with a number, which means it will be the same for all viewport widths, or with an object with `small` and `large` keys.
This will change the value when the viewport width is bigger or smaller than a certain amount of pixels.
The following code would have the component span three rows and four columns on desktop, and one row and two columns on mobile.

`<GridItem colRange={{ small: 2, large: 4 }} rowRange={{ small: 1, large: 3 }}>`
