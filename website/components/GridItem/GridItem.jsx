import { cx } from '../../lib/styleUtils'

// converts an array of react props to global css classes
// for example: props [ { colStart: { small: 1, large: 2} }, { colRange: 2 } ]
// returns [ 'col-start-1', 'col-start-lg-2', 'col-span-2' ]
const propsToClasses = (props) =>
  props.flatMap((prop) => {
    const [key, val] = Object.entries(prop)[0]
    switch (key) {
      case 'colStart': {
        const classes = []
        if (typeof val === 'number') classes.push(`col-start-${val}`)
        if (val?.small) classes.push(`col-start-${val.small}`)
        if (val?.large) classes.push(`col-start-lg-${val.large}`)
        return classes
      }
      case 'colRange': {
        const classes = []
        if (typeof val === 'number') classes.push(`col-span-${val}`)
        if (val?.small) classes.push(`col-span-${val.small}`)
        if (val?.large) classes.push(`col-span-lg-${val.large}`)
        return classes
      }
      case 'rowStart': {
        const classes = []
        if (typeof val === 'number') classes.push(`row-start-${val}`)
        if (val?.small) classes.push(`row-start-${val.small}`)
        if (val?.large) classes.push(`row-start-lg-${val.large}`)
        return classes
      }
      case 'rowRange': {
        const classes = []
        if (typeof val === 'number') classes.push(`row-span-${val}`)
        if (val?.small) classes.push(`row-span-${val.small}`)
        if (val?.large) classes.push(`row-span-lg-${val.large}`)
        return classes
      }
      default:
        return []
    }
  })

const GridItem = ({
  as,
  className: originalClass,
  colStart,
  colRange,
  rowStart,
  rowRange,
  children,
  ...otherProps
}) => {
  const className = cx(
    originalClass,
    propsToClasses([{ colStart }, { colRange }, { rowStart }, { rowRange }]),
  )
  const props = { className, ...otherProps }

  switch (as) {
    case 'section':
      return <section {...props}>{children}</section>
    case 'figure':
      return <figure {...props}>{children}</figure>
    default:
      return <div {...props}>{children}</div>
  }
}

export default GridItem
