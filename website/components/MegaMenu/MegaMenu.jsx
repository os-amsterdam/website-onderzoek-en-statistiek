import { useContext } from 'react'

import HEADER_LINKS from '../../constants/contentLinks'
import CONTENT_TYPES from '../../constants/contentTypes'
import ShortcutContext from '../../lib/ShortcutContext'
import { normalizeItemList } from '../../lib/normalizeUtils'
import { cx } from '../../lib/styleUtils'

import Link from '../Link/Link'

import styles from './MegaMenu.module.css'

const MegaMenu = ({ isOpen, currentPath }) => {
  const shortcuts = useContext(ShortcutContext)

  return (
    <div
      id="menu"
      className={styles.container}
      data-is-open={isOpen || null}
      /*
      Tab index is set to -1 here, so clicking on the MegaMenu will add it to the
      relatedTarget of the Menu list (/website/components/Navigation/Navigation.jsx#L48-50),
      but it won't be tab focusable.
    */
      tabIndex={-1}
    >
      <div className={cx(styles.searchLink, 'mb-40')}>
        <Link
          href="/zoek"
          aria-current={currentPath === '/zoek' && 'page'}
          className="analytics-menu-search-link"
        >
          Zoeken
        </Link>
      </div>

      <div className="col-span-4 col-span-lg-8">
        <h2 className="mb-8" data-style-as="h4">
          Thema’s
        </h2>
        <ul className={cx(styles.column, 'mb-40')}>
          {HEADER_LINKS.themes
            .slice()
            .sort((a, b) => a.label.localeCompare(b.label))
            .map(({ label, slug }) => (
              <li className={styles.menuItem} key={slug}>
                <Link
                  data-variant="inList"
                  href={slug}
                  aria-current={currentPath === slug && 'page'}
                  className="analytics-menu-theme-link"
                >
                  {label}
                </Link>
              </li>
            ))}
        </ul>
      </div>

      <div className="col-span-4">
        <h2 className="mb-8" data-style-as="h4">
          Categorieën
        </h2>
        <ul className="mb-40">
          {Object.values(CONTENT_TYPES)
            .filter((cat) => cat.type !== 'theme')
            .map(({ name, plural }) => (
              <li className={styles.menuItem} key={name}>
                <Link
                  className={cx(
                    styles.lightLink,
                    'analytics-menu-content-type-link',
                  )}
                  data-variant="inList"
                  href={`/zoek?categorie=${name}`}
                  aria-current={
                    currentPath === `/zoek?categorie=${name}` && 'page'
                  }
                >
                  {plural}
                </Link>
              </li>
            ))}
        </ul>

        <h2 className="mb-8" data-style-as="h4">
          Snel naar
        </h2>
        <ul className="mb-40">
          {normalizeItemList(shortcuts).map(({ path, title, shortTitle }) => (
            <li className={styles.menuItem} key={path}>
              <Link
                className={cx(styles.lightLink, 'analytics-menu-shortcut-link')}
                data-variant="inList"
                href={path}
                aria-current={currentPath === path && 'page'}
              >
                {shortTitle || title}
              </Link>
            </li>
          ))}
          <li className={styles.menuItem}>
            <Link
              className={cx(styles.lightLink, 'analytics-menu-shortcut-link')}
              data-variant="inList"
              href="/artikel/over-onderzoek-en-statistiek"
              aria-current={
                currentPath === '/artikel/over-onderzoek-en-statistiek' &&
                'page'
              }
            >
              Over Onderzoek en Statistiek
            </Link>
          </li>
        </ul>
      </div>
    </div>
  )
}

export default MegaMenu
