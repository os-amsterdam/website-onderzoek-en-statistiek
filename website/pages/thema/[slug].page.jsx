import NextImage from 'next/image'
import { useRouter } from 'next/router'
import qs from 'qs'
import dynamic from 'next/dynamic'

import FallbackPage from '../../components/FallbackPage/FallbackPage'
import Seo from '../../components/Seo/Seo'
import Alert from '../../components/Alert/Alert'
import Icon from '../../components/Icon/Icon'
import Link from '../../components/Link/Link'
import { Card, CardHeadingGroup, CardImage } from '../../components/Card/Card'
import ThemeSearch from '../../components/ThemeSearch/ThemeSearch'
import { fetchAPI, getStrapiMedia, PLACEHOLDER_IMAGE } from '../../lib/utils'
import { cx, getFontColor, translateColor } from '../../lib/styleUtils'
import { normalizeItemList } from '../../lib/normalizeUtils'
import CONTENT_TYPES from '../../constants/contentTypes'
import { themeQuery } from '../../lib/queries'

import styles from './theme.module.css'

const Theme = ({
  title,
  slug,
  shortTitle,
  teaser,
  rectangularImage,
  intro,
  visualisation,
  topStory,
  featured,
  collections,
  combiPicture,
  email,
  phoneNumber,
  publishedAt,
}) => {
  const router = useRouter()
  if (router.isFallback) {
    return <FallbackPage />
  }

  return (
    <>
      <Seo
        title={`Thema ${shortTitle || title}`}
        description={teaser || intro}
        image={getStrapiMedia(rectangularImage)}
      />

      <section
        className={cx(styles.container, 'grid')}
        aria-label={`Introductie op thema ${title}`}
      >
        <div className="col-span-4 col-span-lg-10">
          {!publishedAt && <Alert />}
          <h1 className="mb-24 mb-lg-40">{title}</h1>
        </div>
        <div
          className={cx(
            styles.intro,
            'mb-32',
            'mb-lg-0',
            'col-span-4',
            'col-span-lg-5',
          )}
        >
          <p className="mb-24" data-small>
            {intro}
          </p>
          {normalizeItemList(topStory).map(({ path }) => (
            <Link
              className={cx('mb-24', 'mb-lg-40')}
              key={path}
              href={path}
              variant="standalone"
            >
              Lees meer
            </Link>
          ))}
        </div>
      </section>

      {visualisation && (
        <figure
          className={cx(styles.colorBar, 'grid')}
          style={{
            '--color': translateColor(visualisation.color || 'lichtblauw'),
          }}
          aria-label="Visualisatie bij dit thema"
        >
          <div className="col-start-1 col-start-lg-6 col-span-4 col-span-lg-7">
            <div className={styles.chartContainer}>
              <div className={styles.chart}>
                <h2 data-style-as="h5" className="mb-24">
                  {visualisation.title}
                </h2>
                {visualisation.image && (
                  <NextImage
                    src={getStrapiMedia(visualisation.image)}
                    alt={visualisation.altText}
                    width={visualisation.image.width}
                    height={visualisation.image.height}
                    priority
                    sizes="840px"
                    style={{
                      width: '100%',
                      height: 'auto',
                    }}
                  />
                )}
                {!visualisation.image && visualisation.specification && (
                  <os-visualisation
                    suppressHydrationWarning
                    spec={JSON.stringify(visualisation.specification)}
                    altText={visualisation.altText}
                    no-footer
                    no-border
                  ></os-visualisation>
                )}
              </div>
              {visualisation.source && (
                <p
                  className={styles.caption}
                  style={{
                    '--color': getFontColor(visualisation.color),
                  }}
                  data-small
                >
                  {`Bron: ${visualisation.source}`}
                </p>
              )}
            </div>
          </div>
        </figure>
      )}

      {featured.length > 0 && (
        <section
          className={cx(styles.features, 'grid')}
          aria-labelledby="headingFeatured"
        >
          <div className="col-span-4 col-span-lg-12">
            <h2
              id="headingFeatured"
              className="mb-40 mb-lg-24"
              data-style-as="h3"
            >
              Uitgelicht in dit thema
            </h2>
          </div>
          <ul className="grid col-span-4 col-span-lg-12">
            {normalizeItemList(featured).map(
              ({
                path,
                title: featureTitle,
                shortTitle: featureShortTitle,
                teaser: featureTeaser,
                rectangularImage: featureRectangularImage,
                type,
              }) => (
                <li className="col-span-4" key={path}>
                  <Card variant="medium">
                    <CardImage
                      image={featureRectangularImage}
                      type={type}
                      sizes="(max-width: 840px) 840px, 450px"
                    />
                    <CardHeadingGroup>
                      <h3 data-style-as="h5">
                        <Link href={path} data-inline>
                          {featureShortTitle || featureTitle}
                        </Link>
                      </h3>
                      <p data-small className="mt-20">
                        {CONTENT_TYPES[type.toLowerCase()].name}
                      </p>
                    </CardHeadingGroup>
                    <p data-small>{featureTeaser}</p>
                  </Card>
                </li>
              ),
            )}
          </ul>
        </section>
      )}

      {collections.length > 0 && (
        <section
          className={cx(styles.collections, 'grid')}
          aria-labelledby="headingCollections"
        >
          <div className="col-span-5 col-span-lg-16">
            <h2 id="headingCollections" className="mb-40 mb-lg-80">
              Dossiers binnen dit thema
            </h2>
          </div>

          <div className="col-span-4 col-span-lg-6 col-start-1 col-start-lg-7 row-span-1 row-span-lg-3">
            <div className={styles.largeImage}>
              <NextImage
                src={
                  combiPicture?.biggerImage
                    ? getStrapiMedia(combiPicture.biggerImage)
                    : PLACEHOLDER_IMAGE
                }
                alt=""
                fill
                sizes="(max-width: 840px) 840px, 680px"
                style={{
                  objectFit: 'cover',
                }}
              />
              <div className={styles.smallImage}>
                <NextImage
                  src={
                    combiPicture?.smallerImage
                      ? getStrapiMedia(combiPicture.smallerImage)
                      : PLACEHOLDER_IMAGE
                  }
                  alt=""
                  fill
                  sizes="280px"
                  style={{
                    objectFit: 'cover',
                  }}
                />
              </div>
            </div>
          </div>

          <div className="col-span-4 col-span-lg-5">
            <ul className="mb-40" data-columns={collections.length > 5 || 0}>
              {collections
                .sort(
                  (a, b) => Date.parse(b.updatedAt) - Date.parse(a.updatedAt),
                )
                .slice(0, 10)
                .map(({ title: linkTitle, slug: linkSlug }) => (
                  <li key={linkSlug}>
                    <h3>
                      <Link href={`/dossier/${linkSlug}`} variant="inList">
                        <Icon className="mr-12" size={14} />
                        {linkTitle}
                      </Link>
                    </h3>
                  </li>
                ))}
            </ul>
            {collections.length > 10 && (
              <Link
                href={`/zoek?thema=${slug}&categorie=dossier`}
                variant="standalone"
              >
                Alle dossiers met dit thema
              </Link>
            )}
          </div>
        </section>
      )}

      <section className={cx(styles.container, 'grid')}>
        <div className="col-start-1 col-start-lg-3 col-span-4 col-span-lg-8">
          <ThemeSearch themeTitle={title} slug={slug} />
        </div>

        {(email || phoneNumber) && (
          <section
            className="col-span-4 mb-32 mb-lg-80"
            aria-labelledby="headingContactTheme"
          >
            <h2
              id="headingContactTheme"
              className="mb-8 mb-lg-40"
              data-style-as="h3"
            >
              Heeft u een vraag over dit thema?
            </h2>
            <p data-small>
              Neem contact met ons op. U kunt ons bereiken via{' '}
              {email && (
                <Link
                  href={`mailto:${email}`}
                  variant="inline"
                  data-small
                  external
                >
                  e-mail
                </Link>
              )}
              {email && phoneNumber && ' of '}
              {phoneNumber && (
                <Link href={`tel:${phoneNumber}`} variant="inline" external>
                  {phoneNumber}
                </Link>
              )}
              .
            </p>
          </section>
        )}
      </section>
    </>
  )
}

export async function getStaticPaths() {
  const themes = await fetchAPI('/api/themes')

  return {
    paths: themes.data.map(({ slug }) => ({
      params: { slug },
    })),
    fallback: true,
  }
}

export async function getStaticProps({ params, preview }) {
  const data = await fetchAPI(
    `/api/themes?${qs.stringify(
      {
        filters: { slug: { $eq: params.slug } },
        ...themeQuery,
      },
      {
        encodeValuesOnly: true,
      },
    )}${preview ? '&status=draft' : ''}`,
  )

  if (!data.data[0]) {
    return {
      notFound: true,
      revalidate: 10,
    }
  }

  return {
    props: data.data[0],
    revalidate: 10,
  }
}

export default Theme
