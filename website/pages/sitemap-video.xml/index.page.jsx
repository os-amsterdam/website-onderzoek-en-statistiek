import { fetchAPI, prependRootURL, prependStrapiURL } from '../../lib/utils'

export const getServerSideProps = async ({ res }) => {
  const videos = await fetchAPI('/api/videos?fields=id').then((metaResult) =>
    fetchAPI(
      `/api/videos?populate=*&pagination[pageSize]=${metaResult.meta.pagination.total}`,
    ),
  )

  const sitemap = `<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">
    ${videos.data
      .filter(({ videoFile }) => videoFile)
      .map(
        ({
          slug,
          publicationDate,
          title,
          teaser,
          rectangularImage,
          videoFile,
        }) =>
          `
        <url>
          <loc>${prependRootURL(`/video/${slug}`)}</loc>
          <video:video>
            <video:title>${title}</video:title>
            <video:description>${teaser}</video:description>
            <video:thumbnail_loc>${
              rectangularImage
                ? prependStrapiURL(rectangularImage.url)
                : 'https://onderzoek.amsterdam.nl/default_image.jpg'
            }</video:thumbnail_loc>
            <video:content_loc>${prependStrapiURL(
              videoFile.url,
            )}</video:content_loc>
            <video:publication_date>${
              publicationDate.split('T')[0] || undefined
            }</video:publication_date>
            <video:live>no</video:live>
          </video:video>
        </url>
      `,
      )
      .join('')}
    </urlset>
  `

  res.setHeader('Cache-Control', 's-maxage=30, stale-while-revalidate')
  res.setHeader('Content-Type', 'text/xml')
  res.write(sitemap)

  res.end()

  // Empty since we don't render anything
  return {
    props: {},
  }
}

// Default export to prevent next.js errors
const SitemapXML = () => {}

export default SitemapXML
