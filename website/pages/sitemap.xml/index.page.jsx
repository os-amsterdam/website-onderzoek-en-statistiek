import { getSearchContent } from '../../lib/searchUtils'
import { prependRootURL } from '../../lib/utils'
import CONTENT_TYPES from '../../constants/contentTypes'
import HEADER_LINKS from '../../constants/contentLinks'

// fix publication dates that pre date the internet :)
const lastmod = (publicationDate) =>
  new Date(publicationDate) > new Date('1970-01-01')
    ? publicationDate.split('T')[0]
    : '1970-01-01'

export const getServerSideProps = async ({ res }) => {
  const content = await getSearchContent()
  const today = new Date(Date.now()).toISOString().split('T')[0]

  const sitemap = `<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
      <url>
        <loc>${prependRootURL()}</loc>
        <lastmod>${today}</lastmod> 
      </url>
    ${HEADER_LINKS.themes
      .map(
        ({ slug }) =>
          `<url>
        <loc>${prependRootURL(slug)}</loc>
        <lastmod>${today}</lastmod>
      </url>
      `,
      )
      .join('')}
    ${content
      .map(
        ({ slug, publicationDate, type }) =>
          `<url>
        <loc>${prependRootURL(`/${CONTENT_TYPES[type].name}/${slug}`)}</loc>
        <lastmod>${lastmod(publicationDate)}</lastmod>
      </url>
      `,
      )
      .join('')}
    </urlset>
  `

  res.setHeader('Cache-Control', 's-maxage=30, stale-while-revalidate')
  res.setHeader('Content-Type', 'text/xml')
  res.write(sitemap)

  res.end()

  // Empty since we don't render anything
  return {
    props: {},
  }
}

// Default export to prevent next.js errors
const SitemapXML = () => {}

export default SitemapXML
