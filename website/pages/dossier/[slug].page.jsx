import { useState } from 'react'
import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import qs from 'qs'

import FallbackPage from '../../components/FallbackPage/FallbackPage'
import Seo from '../../components/Seo/Seo'
import { fetchAPI, getStrapiMedia, formatDate } from '../../lib/utils'
import { normalizeItemList } from '../../lib/normalizeUtils'
import { Card, CardHeadingGroup, CardImage } from '../../components/Card/Card'
import Icon from '../../components/Icon/Icon'
import Alert from '../../components/Alert/Alert'
import Link from '../../components/Link/Link'
import ContentFooter from '../../components/ContentFooter/ContentFooter'
import { cx } from '../../lib/styleUtils'
import CONTENT_TYPES from '../../constants/contentTypes'
import { collectionQuery } from '../../lib/queries'

import styles from './collection.module.css'

const BodyContent = dynamic(
  () => import('../../components/BodyContent/BodyContent'),
)

const Collection = ({
  title,
  shortTitle,
  teaser,
  rectangularImage,
  intro,
  body,
  featured,
  collectionItems: collectionItemsCms,
  linkList,
  email,
  phoneNumber,
  themes,
  toc,
  publishedAt,
}) => {
  const [showAll, setShowAll] = useState(false)
  const router = useRouter()
  if (router.isFallback) {
    return <FallbackPage />
  }

  const collectionItems = normalizeItemList(collectionItemsCms)
    .slice()
    .map((d) => ({ ...d, date: d.publicationDate || d.updatedAt }))
    .sort((a, b) => new Date(b.date) - new Date(a.date))
    .map(
      ({
        path,
        title: featureTitle,
        shortTitle: featureShortTitle,
        teaser: featureTeaser,
        type,
        date,
        dateConfig,
      }) => {
        const formattedDate = formatDate(date, dateConfig)
        return (
          <li key={path} className="mb-40">
            <Card>
              <CardHeadingGroup>
                <h3 data-style-as="h5">
                  <Link href={path} data-inline>
                    {featureShortTitle || featureTitle}
                  </Link>
                </h3>
                <p data-small>{CONTENT_TYPES[type.toLowerCase()].name}</p>
              </CardHeadingGroup>
              <p className="mb-8">{featureTeaser}</p>
              <p data-small>
                {type === 'collection' ||
                type === 'dataset' ||
                type === 'interactive'
                  ? `Laatst gewijzigd: ${formattedDate}`
                  : `Gepubliceerd: ${formattedDate}`}
              </p>
            </Card>
          </li>
        )
      },
    )

  const normalizedFeatures = normalizeItemList(featured.slice(0, 4))

  return (
    <>
      <Seo
        title={`Dossier: ${shortTitle || title}`}
        description={teaser || intro}
        image={getStrapiMedia(rectangularImage)}
      />
      <div className={cx(styles.container, 'grid')}>
        <section
          className="col-start-1 col-start-lg-3 col-span-4 col-span-lg-9"
          aria-label={`Introductie op dossier ${title}`}
        >
          {!publishedAt && <Alert />}
          <h1 className="mb-24">{title}</h1>
          <p className="mb-72 mb-lg-120" data-large>
            {intro}
          </p>
        </section>

        {featured.length > 0 && (
          <section
            className="col-span-4 col-span-lg-12 mb-32 mb-lg-80"
            aria-labelledby="headingFeatured"
          >
            <h2 id="headingFeatured" className="hide-visually">
              Uitgelicht in dit dossier
            </h2>
            <ul className={styles.features}>
              {normalizedFeatures.map(
                ({
                  path,
                  title: featureTitle,
                  shortTitle: featureShortTitle,
                  teaser: featureTeaser,
                  rectangularImage: featureTeaserImage,
                  type,
                }) => (
                  <li className={styles.feature} key={path}>
                    <Card variant="medium">
                      <CardImage
                        aspectRatio="16-9"
                        image={featureTeaserImage}
                        type={type}
                        sizes="(max-width: 840px) 840px, 680px"
                      />
                      <CardHeadingGroup>
                        <h3 data-style-as="h5">
                          <Link href={path} data-inline>
                            {featureShortTitle || featureTitle}
                          </Link>
                        </h3>
                        <p className="mt-20" data-small>
                          {CONTENT_TYPES[type.toLowerCase()].name}
                        </p>
                      </CardHeadingGroup>
                      <p data-small>{featureTeaser}</p>
                    </Card>
                  </li>
                ),
              )}
            </ul>
          </section>
        )}

        {body && <BodyContent content={body} toc={toc} />}
      </div>

      <div className={cx(styles.container, 'grid')}>
        {collectionItems.length > 0 && (
          <>
            <div className="col-start-1 col-start-lg-5 col-span-4 col-span-lg-8">
              <h2
                id="headingAllContent"
                className="mb-40"
              >{`Alles in ${title}`}</h2>
            </div>
            <section
              aria-labelledby="headingAllContent"
              className="col-start-1 col-start-lg-5 col-span-4 col-span-lg-8 mb-32 mb-lg-40"
            >
              <ul>{collectionItems.slice(0, 5)}</ul>
              {collectionItems.length > 5 && (
                <>
                  {showAll && <ul>{collectionItems.slice(5)}</ul>}
                  <button data-primary onClick={() => setShowAll(!showAll)}>
                    <Icon
                      class={styles.toggle}
                      data-is-open={showAll || null}
                      name="chevron-down"
                      size={24}
                    ></Icon>
                    {showAll ? 'Toon minder' : 'Toon meer'}
                  </button>
                </>
              )}
            </section>
          </>
        )}

        {linkList.length > 0 && (
          <aside
            aria-labelledby="headingRelatedContent"
            className="col-start-1 col-span-4 row-start-3 row-start-lg-2 mb-32 mb-lg-40"
          >
            <h2 id="headingRelatedContent" data-style-as="h5" className="mb-12">
              Zie ook
            </h2>
            <ul>
              {normalizeItemList(linkList).map(
                ({ name, path, title: linkTitle }) => (
                  <li key={path}>
                    <Link
                      href={path}
                      variant="inList"
                      external={name === 'link'}
                    >
                      <Icon name="chevron-right" className="mr-12" size={14} />
                      {linkTitle}
                    </Link>
                  </li>
                ),
              )}
            </ul>
          </aside>
        )}

        <div className="col-start-1 col-start-lg-5 col-span-4 col-span-lg-8">
          <ContentFooter type="dossier" themes={themes} />
        </div>

        {(email || phoneNumber) && (
          <section
            aria-labelledby="headingContactCollection"
            className="col-span-4 row-start-lg-4 mb-32 mb-lg-40"
          >
            <h2
              id="headingContactCollection"
              className="mb-12 mb-lg-40"
              data-style-as="h3"
            >
              Heeft u een vraag over dit dossier?
            </h2>
            <p data-small>
              Neem contact met ons op. U kunt ons bereiken via{' '}
              {email && (
                <Link href={`mailto:${email}`} variant="inline" external>
                  e-mail
                </Link>
              )}
              {email && phoneNumber && ' of '}
              {phoneNumber && (
                <Link href={`tel:${phoneNumber}`} variant="inline" external>
                  {phoneNumber}
                </Link>
              )}
              .
            </p>
          </section>
        )}
      </div>
    </>
  )
}

export async function getStaticPaths() {
  const collections = await fetchAPI('/api/collections?fields=id').then(
    (metaResult) =>
      fetchAPI(
        `/api/collections?fields=slug&pagination[pageSize]=${metaResult.meta.pagination.total}`,
      ),
  )

  return {
    paths: collections.data.map(({ slug }) => ({
      params: {
        slug,
      },
    })),
    fallback: true,
  }
}

export async function getStaticProps({ params, preview }) {
  const data = await fetchAPI(
    `/api/collections?${qs.stringify(
      {
        filters: { slug: { $eq: params.slug } },
        ...collectionQuery,
      },
      {
        encodeValuesOnly: true,
      },
    )}${preview ? '&status=draft' : ''}`,
  )

  if (!data.data[0]) {
    return {
      notFound: true,
      revalidate: 10,
    }
  }

  return {
    props: data.data[0],
    revalidate: 10,
  }
}

export default Collection
