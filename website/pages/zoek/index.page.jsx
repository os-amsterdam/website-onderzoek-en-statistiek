import {
  useState,
  useEffect,
  useContext,
  useRef,
  useCallback,
  startTransition,
} from 'react'
import { useRouter } from 'next/router'
import debounce from 'lodash.debounce'
import qs from 'qs'

import Seo from '../../components/Seo/Seo'
import Icon from '../../components/Icon/Icon'
import SkeletonLoading from '../../components/SkeletonLoading/SkeletonLoading'
import SearchBar from '../../components/SearchBar/SearchBar'
import SearchResults from '../../components/SearchResults/SearchResults'
import SearchFilters from '../../components/SearchFilters/SearchFilters'
import useIsMobile from '../../lib/useIsMobile'
import {
  translateContentType,
  decodeQuerySafe,
  fetchAPI,
} from '../../lib/utils'
import { trackSearchQuery } from '../../lib/analyticsUtils'
import CONTENT_TYPES from '../../constants/contentTypes'
import {
  SearchContext,
  getSearchResults,
  getPeriodRange,
  showPeriod,
  sanitizePeriodParamString,
} from '../../lib/searchUtils'
import { cx } from '../../lib/styleUtils'

import styles from './search.module.css'

const Search = ({ themes }) => {
  const searchIndex = useContext(SearchContext)
  const router = useRouter()

  const [searchQuery, setSearchQuery] = useState('')
  const [category, setCategory] = useState('')
  const [themeFilter, setThemeFilter] = useState([])
  const [page, setPage] = useState(1)
  const [period, setPeriod] = useState([null, null])
  const [results, setResults] = useState()

  const periodRange = getPeriodRange(searchIndex)

  const setQueries = (q, cat, theme, pageNumber, selectedPeriod) => {
    setSearchQuery(q ? decodeQuerySafe(q) : '')
    setCategory(translateContentType(cat) || '')
    setThemeFilter(
      themes?.some(({ slug }) => theme?.includes(slug)) ? theme.split(' ') : [],
    )
    setPage(pageNumber ? parseInt(pageNumber, 10) : 1)
    setPeriod(
      selectedPeriod
        ? sanitizePeriodParamString(selectedPeriod, periodRange)
        : '',
    )
  }

  // get state from url params on first render
  // or when periodRange or category changes
  useEffect(() => {
    if (router.isReady) {
      const {
        tekst: q,
        categorie: cat,
        thema: theme,
        pagina: pageNumber,
        periode: selectedPeriod,
      } = router.query

      setQueries(q, cat, theme, pageNumber, selectedPeriod)
    }
  }, [router.isReady, router.query.categorie, periodRange[0]])

  // get state from url params on forward and back button click
  useEffect(() => {
    router.beforePopState(({ as }) => {
      if (as !== router.asPath) {
        const queryString = as.substring(as.indexOf('?'))
        const urlParams = new URLSearchParams(queryString)

        const q = urlParams.get('tekst')
        const cat = urlParams.get('categorie')
        const theme = urlParams.get('thema')
        const pageNumber = urlParams.get('pagina')
        const selectedPeriod = urlParams.get('periode')

        setQueries(q, cat, theme, pageNumber, selectedPeriod)
      }
      return true
    })

    return () => {
      router.beforePopState(() => true)
    }
  }, [router])

  // push state to url params
  useEffect(() => {
    const throttledUpdate = debounce(() => {
      const query = {
        ...(searchQuery !== '' && { tekst: searchQuery }),
        ...(category && { categorie: CONTENT_TYPES[category].name }),
        ...(themeFilter.length > 0 && { thema: themeFilter.join(' ') }),
        ...(page !== 1 && { pagina: page }),
        ...(showPeriod(period, periodRange) && {
          periode: `${period[0]}-${period[1]}`,
        }),
      }

      router.replace({ query }, undefined, {
        shallow: true,
        scroll: false,
      })
    }, 300)

    // only push state to url params after router is ready, so empty state
    // isn't pushed before we get a chance to get state from url params
    if (router.isReady) {
      throttledUpdate()
    }
    return () => throttledUpdate.cancel()
  }, [searchQuery, category, themeFilter, page, period])

  useEffect(() => {
    const throttledUpdate = debounce(() => {
      // startTransition makes these calls a lower priority then all other calls,
      // to make sure typing in the text input is smooth
      startTransition(() => {
        const updatedResults = getSearchResults(
          searchIndex,
          searchQuery,
          themeFilter,
          category,
          period,
        )
        setResults(updatedResults)
      })
    }, 300)

    throttledUpdate()

    return () => throttledUpdate.cancel()
  }, [searchIndex, searchQuery, themeFilter, category, period])

  const handleThemeChange = (slug) => {
    const newThemeFilter = themeFilter.includes(slug)
      ? themeFilter.filter((item) => item !== slug)
      : [...themeFilter, slug]

    setThemeFilter(newThemeFilter)
    setPage(1)
  }

  useEffect(() => {
    const hasQueryOrFilter = Boolean(
      searchQuery ||
        category ||
        themeFilter.length > 0 ||
        showPeriod(period, periodRange),
    )

    if (hasQueryOrFilter) {
      const customDimensions = {
        dimension1: 'main',
        dimension2:
          themeFilter.length > 0 ? themeFilter.join(', ') : 'no filter',
        dimension3: showPeriod(period, periodRange)
          ? `${period[0]}-${period[1]}`
          : 'no filter',
        dimension4: page,
      }

      const tracker = trackSearchQuery(
        searchQuery || 'no query',
        category || 'no filter',
        undefined,
        customDimensions,
      )
      return () => tracker.cancel()
    }
    return undefined
  }, [searchQuery, category, themeFilter, period, page])

  /* Observe and handle changes in custom pagination element  */
  const firstResultRef = useRef()

  const updateCurrentPage = (record) => {
    const mutation = record[0]

    const newValue = mutation.target.attributes.getNamedItem(
      mutation.attributeName,
    ).value
    setPage(+newValue)

    window.scrollTo({
      top: 0,
      left: 0,
      behavior: 'instant',
    })

    // set focus on first result
    if (firstResultRef.current) {
      firstResultRef.current.focus()
    }
  }

  const paginationRef = useCallback((node) => {
    if (node !== null) {
      const observer = new MutationObserver(updateCurrentPage)
      observer.observe(node, {
        attributes: true,
        attributeFilter: ['current'],
      })
    }
  }, [])

  const totalPages = results ? Math.ceil(results.length / 10) : 0

  const isMobile = useIsMobile()

  return (
    <>
      <Seo
        title="Zoeken"
        description="Zoek naar artikelen, publicaties, factsheets, animaties, dashboards en cijfers op de website van Onderzoek en Statistiek."
      />
      <div className={cx(styles.container, 'grid')}>
        <div className="col-span-4 col-span-lg-7 col-start-1 col-start-lg-5">
          <hgroup className={styles.header}>
            <h1 className="mb-24 mb-lg-16" data-style-as="h2">
              Zoeken
            </h1>
            {results ? (
              <p className="mb-8" aria-live="polite">
                {`${
                  page > 1 ? `Pagina ${page} van ` : ''
                } ${results.length.toLocaleString('nl-NL')} ${
                  results.length === 1 ? 'resultaat' : 'resultaten'
                }`}
              </p>
            ) : (
              <SkeletonLoading
                width="160px"
                // Because we use fluid typography, the line height of the text differs depending on viewport width.
                // The same function is used here to calculate the height of the loading element
                height="var(--level-3-font-size)"
                className="mb-8"
              />
            )}
          </hgroup>
          <SearchBar
            id="searchfield"
            role="search"
            value={searchQuery}
            onChange={(q) => {
              setSearchQuery(q)
              setPage(1)
            }}
            autoFocus={!isMobile}
          />
        </div>
        <div className="col-span-4 col-span-lg-8 col-start-1 col-start-lg-5">
          <section className={styles.tags} aria-label="Actieve filters">
            <ul>
              {Object.values(CONTENT_TYPES)
                .filter((cat) => cat.type === category)
                .map(({ type, plural }) => (
                  <li key={type}>
                    <button
                      type="button"
                      data-primary
                      aria-label={`Geselecteerde categorie: ${plural}. Klik om filter te verwijderen.`}
                      data-small
                      onClick={() => {
                        setCategory('')
                        setPage(1)
                      }}
                    >
                      <Icon className="mr-16" size={24} name="close" />
                      <span>{plural}</span>
                    </button>
                  </li>
                ))}
              {showPeriod(period, periodRange) && (
                <li>
                  <button
                    type="button"
                    data-primary
                    aria-label={`Geselecteerde periode: van ${period[0]} tot ${period[1]}. Klik om filter te verwijderen.`}
                    data-small
                    onClick={() => {
                      setPeriod([null, null])
                      setPage(1)
                    }}
                  >
                    <Icon className="mr-16" size={24} name="close" />
                    <span>{`${period[0]}-${period[1]}`}</span>
                  </button>
                </li>
              )}
              {themes
                .filter(({ slug }) => themeFilter.includes(slug))
                .map(({ title, slug }) => (
                  <li key={slug}>
                    <button
                      type="button"
                      aria-label={`Geselecteerd thema: ${title}. Klik om filter te verwijderen.`}
                      data-secondary
                      data-small
                      onClick={() => handleThemeChange(slug)}
                    >
                      <Icon className="mr-16" size={24} name="close" />
                      {title}
                    </button>
                  </li>
                ))}
            </ul>
          </section>
          <section className="mt-16" aria-labelledby="headingSearchResults">
            <h2 id="headingSearchResults" className="hide-visually">
              Zoekresultaten
            </h2>
            <SearchResults
              results={results}
              page={page}
              cardHeadingLevel="h3"
              firstResultRef={firstResultRef}
            />
          </section>
        </div>
        <div className="col-span-4 col-span-lg-8 col-start-1 col-start-lg-5">
          <os-pagination
            class={styles.pagination}
            ref={paginationRef}
            current={page}
            total={totalPages}
          />
        </div>
        <SearchFilters
          results={results}
          themes={themes}
          themeFilter={themeFilter}
          handleThemeChange={handleThemeChange}
          category={category}
          setCategory={setCategory}
          setPage={setPage}
          period={period}
          setPeriod={setPeriod}
          periodRange={periodRange}
        />
      </div>
    </>
  )
}

export async function getStaticProps() {
  const themeQuery = qs.stringify(
    { fields: ['title', 'slug'] },
    { encodeValuesOnly: true },
  )

  const themes = await fetchAPI(`/api/themes?${themeQuery}`)

  return {
    props: { themes: themes.data },
    revalidate: 10,
  }
}

export default Search
