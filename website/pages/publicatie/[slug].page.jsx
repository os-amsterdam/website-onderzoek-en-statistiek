import dynamic from 'next/dynamic'
import NextImage from 'next/image'
import { useRouter } from 'next/router'
import qs from 'qs'

import FallbackPage from '../../components/FallbackPage/FallbackPage'
import Seo from '../../components/Seo/Seo'
import Alert from '../../components/Alert/Alert'
import ContentFooter from '../../components/ContentFooter/ContentFooter'
import DownloadButton from '../../components/DownloadButton/DownloadButton'
import ByLine from '../../components/ByLine/ByLine'
import {
  fetchAPI,
  getStrapiMedia,
  PLACEHOLDER_IMAGE,
  formatDate,
  formatBytes,
  dateConfig,
} from '../../lib/utils'
import { cx } from '../../lib/styleUtils'
import { publicationQuery } from '../../lib/queries'

import styles from './publication.module.css'

const BodyContent = dynamic(
  () => import('../../components/BodyContent/BodyContent'),
)

const DownloadButtons = ({ links }) => (
  <ul className={styles.buttons}>
    {links.map((file) => (
      <li key={file.url}>
        <DownloadButton
          url={getStrapiMedia(file)}
          data-small
          data-as-link
          data-variant="inList"
        >
          {`${file.name} (${formatBytes(file.size * 1000)})`}
        </DownloadButton>
      </li>
    ))}
  </ul>
)

const Publication = ({
  title,
  shortTitle,
  teaser,
  publicationDate,
  formatPublicationDate,
  author,
  intro,
  body,
  file,
  coverImage,
  rectangularImage,
  themes,
  toc,
  publishedAt,
}) => {
  const router = useRouter()
  if (router.isFallback) {
    return <FallbackPage />
  }

  const MAX_DOWNLOAD_LINKS = 3

  const fileSorter = (a, b) => a.name.localeCompare(b.name)
  const files = Array.isArray(file)
    ? file.sort(fileSorter)
    : [file].sort(fileSorter)

  return (
    <>
      <Seo
        title={shortTitle || title}
        description={teaser || intro}
        image={getStrapiMedia(rectangularImage || coverImage)}
        article
      />

      <div className={cx(styles.container, 'grid')}>
        <div className="col-start-1 col-start-lg-2 col-span-4 col-span-lg-10">
          {!publishedAt && <Alert />}
          <h1>{title}</h1>
          <ByLine
            className={cx('mb-56', 'mb-lg-40')}
            items={[
              'Publicatie',
              author,
              formatDate(publicationDate, dateConfig(formatPublicationDate)),
            ]}
          />
        </div>

        <div className="col-start-1 col-start-lg-2 col-span-4 col-span-lg-6 mb-40">
          <p className={cx(files.length > 1 && 'mb-40')} data-large>
            {intro}
          </p>

          {files.length > 1 && (
            <div className={styles.multipleDownloads}>
              <div>
                <h2 data-style-as="h5" className="mb-12">
                  Downloads
                </h2>
                <DownloadButtons links={files.slice(0, MAX_DOWNLOAD_LINKS)} />
                {files.length > MAX_DOWNLOAD_LINKS && (
                  <details>
                    <summary data-as-link className="mb-12">
                      Toon meer
                    </summary>
                    <DownloadButtons links={files.slice(MAX_DOWNLOAD_LINKS)} />
                  </details>
                )}
              </div>
            </div>
          )}
        </div>

        <div
          className={cx(
            styles.cover,
            'col-start-1',
            'col-start-lg-9',
            'col-span-4',
            'col-span-lg-3',
            'mb-56 mb-lg-40',
          )}
        >
          {coverImage && (
            <div>
              <NextImage
                src={getStrapiMedia(coverImage)}
                alt=""
                width={coverImage.width}
                height={coverImage.height}
                placeholder="blur"
                blurDataURL={PLACEHOLDER_IMAGE}
                priority
                sizes="(max-width: 840px) 840px, 340px"
                style={{
                  display: 'block',
                  width: '100%',
                  height: 'auto',
                  objectFit: 'cover',
                }}
              />
            </div>
          )}
          {files.length === 1 && (
            <div>
              <DownloadButton
                url={getStrapiMedia(files[0])}
                data-small
                data-variant="primary"
              >
                {`Download PDF (${formatBytes(files[0].size * 1000)})`}
              </DownloadButton>
            </div>
          )}
        </div>

        {body && <BodyContent content={body} toc={toc} />}

        <div className="col-start-1 col-start-lg-3 col-span-4 col-span-lg-8">
          <ContentFooter type="publicatie" themes={themes} />
        </div>
      </div>
    </>
  )
}

export async function getStaticPaths() {
  const publications = await fetchAPI('/api/publications?fields=id').then(
    (metaResult) =>
      fetchAPI(
        `/api/publications?fields=slug&pagination[pageSize]=${metaResult.meta.pagination.total}`,
      ),
  )

  return {
    paths: publications.data.map(({ slug }) => ({
      params: {
        slug,
      },
    })),
    fallback: true,
  }
}

export async function getStaticProps({ params, preview }) {
  const data = await fetchAPI(
    `/api/publications?${qs.stringify(
      {
        filters: { slug: { $eq: params.slug } },
        ...publicationQuery,
      },
      {
        encodeValuesOnly: true,
      },
    )}${preview ? '&status=draft' : ''}`,
  )

  if (!data.data[0]) {
    return {
      notFound: true,
      revalidate: 10,
    }
  }

  return {
    props: data.data[0],
    revalidate: 10,
  }
}

export default Publication
