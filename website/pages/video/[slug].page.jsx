import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import Image from 'next/image'
import qs from 'qs'

import FallbackPage from '../../components/FallbackPage/FallbackPage'
import Seo from '../../components/Seo/Seo'
import ByLine from '../../components/ByLine/ByLine'
import Alert from '../../components/Alert/Alert'
import ContentFooter from '../../components/ContentFooter/ContentFooter'
import {
  fetchAPI,
  getStrapiMedia,
  formatDate,
  PLACEHOLDER_IMAGE,
} from '../../lib/utils'
import { cx } from '../../lib/styleUtils'
import { videoQuery } from '../../lib/queries'

import styles from './video.module.css'

const BodyContent = dynamic(
  () => import('../../components/BodyContent/BodyContent'),
)
const ExternalVideo = dynamic(() =>
  import('../../components/VideoPlayer/VideoPlayer').then(
    (mod) => mod.ExternalVideo,
  ),
)
const LocalVideo = dynamic(() =>
  import('../../components/VideoPlayer/VideoPlayer').then(
    (mod) => mod.LocalVideo,
  ),
)
const VideoButtons = dynamic(
  () => import('../../components/VideoButtons/VideoButtons'),
)

const ExternalEmbed = ({ source }) => (
  <iframe className={styles.iframe} title="other-embed" src={source} />
)

const Video = ({
  title,
  shortTitle,
  teaser,
  rectangularImage,
  publicationDate,
  transcript,
  author,
  intro,
  body,
  videoFile,
  subtitleFile,
  wideVideo,
  subtitleDefault,
  externalVideoSource,
  externalEmbedSource,
  themes,
  toc,
  publishedAt,
}) => {
  const router = useRouter()
  if (router.isFallback) {
    return <FallbackPage />
  }

  return (
    <>
      <Seo
        title={shortTitle || title}
        description={teaser || intro}
        image={getStrapiMedia(rectangularImage)}
        video
      />
      <div className={cx(styles.container, 'grid')}>
        <div className="col-start-1 col-start-lg-2 col-span-4 col-span-lg-10">
          {!publishedAt && <Alert />}
          <h1 className="mb-16">{title}</h1>
          <ByLine
            className={cx('mb-56', 'mb-lg-40')}
            items={[author, formatDate(publicationDate)]}
          />
        </div>

        <div
          className={cx(
            styles.video,
            'col-start-1',
            wideVideo ? 'col-start-lg-1' : 'col-start-lg-3',
            'col-span-4',
            wideVideo ? 'col-span-lg-12' : 'col-span-lg-8',
            !(transcript || videoFile) && 'mb-56 mb-lg-80',
          )}
          data-wide={wideVideo || null}
        >
          {videoFile?.url ? (
            <LocalVideo
              videoSource={videoFile}
              subtitleSource={subtitleFile}
              enableSubtitleByDefault={subtitleDefault}
            />
          ) : (
            externalVideoSource && (
              <ExternalVideo source={externalVideoSource} />
            )
          )}
        </div>

        <div className="col-start-1 col-start-lg-3 col-span-4 col-span-lg-8">
          {(transcript || videoFile) && (
            <VideoButtons transcript={transcript} file={videoFile} />
          )}
          {externalEmbedSource && (
            <ExternalEmbed source={externalEmbedSource} />
          )}
          <p className="mb-40 mb-lg-80" data-large>
            {intro}
          </p>
          {!videoFile?.url && !externalVideoSource && rectangularImage && (
            <div className="mb-56 mb-lg-80">
              <Image
                src={getStrapiMedia(rectangularImage)}
                alt=""
                width={rectangularImage.width}
                height={rectangularImage.height}
                placeholder="blur"
                blurDataURL={PLACEHOLDER_IMAGE}
                priority
                sizes="(max-width: 840px) 840px, 920px"
                style={{
                  width: '100%',
                  height: 'auto',
                }}
              />
              {rectangularImage.caption &&
                /*
                  Strapi autofills the 'caption' field with the name of the file.
                  This regex checks if 'caption' doesn't end in an image extension.
                */
                !/\.(jpg|jpeg|png|webp|avif|gif|svg|ico)$/.test(
                  rectangularImage.caption,
                ) && (
                  <p className="mt-8" data-small>
                    {rectangularImage.caption}
                  </p>
                )}
            </div>
          )}
        </div>

        {body && <BodyContent content={body} toc={toc} />}

        <div className="col-start-1 col-start-lg-3 col-span-4 col-span-lg-8">
          <ContentFooter type="video" themes={themes} />
        </div>
      </div>
    </>
  )
}

export async function getStaticPaths() {
  const videos = await fetchAPI('/api/videos?fields=id').then((metaResult) =>
    fetchAPI(
      `/api/videos?fields=slug&pagination[pageSize]=${metaResult.meta.pagination.total}`,
    ),
  )

  return {
    paths: videos.data.map(({ slug }) => ({
      params: {
        slug,
      },
    })),
    fallback: true,
  }
}

export async function getStaticProps({ params, preview }) {
  const data = await fetchAPI(
    `/api/videos?${qs.stringify(
      {
        filters: { slug: { $eq: params.slug } },
        ...videoQuery,
      },
      {
        encodeValuesOnly: true,
      },
    )}${preview ? '&status=draft' : ''}`,
  )

  if (!data.data[0]) {
    return {
      notFound: true,
      revalidate: 10,
    }
  }

  return {
    props: data.data[0],
    revalidate: 10,
  }
}

export default Video
