import qs from 'qs'

import { fetchAPI } from '../lib/utils'
import { cx } from '../lib/styleUtils'
import { normalizeItemList } from '../lib/normalizeUtils'
import Seo from '../components/Seo/Seo'
import Icon from '../components/Icon/Icon'
import Link from '../components/Link/Link'
import { Card, CardImage, CardHeadingGroup } from '../components/Card/Card'
import CONTENT_TYPES from '../constants/contentTypes'

import styles from './homepage.module.css'
import { homepageQuery, themesQuery } from '../lib/queries'

const Home = ({ themes, homepage }) => {
  if (!themes) return <div>Refresh page</div>

  const {
    largeFirstFeature,
    featured,
    agenda,
    shortcuts,
    relatedSites,
    featuredCollections,
  } = homepage

  const flatFeatures = normalizeItemList(featured)

  const agendaList = agenda.length > 0 && (
    <>
      <h3 className="mb-12" data-style-as="h5">
        Agenda
      </h3>
      <ul className="mb-56">
        {normalizeItemList(agenda).map(({ path, title, shortTitle }) => (
          <li className="mb-12" key={path}>
            <Link href={path} data-variant="inList">
              <Icon name="chevron-right" size="14px"></Icon>
              {shortTitle || title}
            </Link>
          </li>
        ))}
      </ul>
    </>
  )

  const relatedSiteList = relatedSites.length > 0 && (
    <>
      <h3 className="mb-12" data-style-as="h5">
        Meer feiten en cijfers
      </h3>
      <ul>
        {relatedSites.map(({ path, title }) => (
          <li className="mb-12" key={path}>
            <Link href={path} data-variant="inList" external>
              <Icon name="external-link" size="14px"></Icon>
              {title}
            </Link>
          </li>
        ))}
      </ul>
    </>
  )

  return (
    <div className={styles.container}>
      <Seo />
      <div className="grid mb-40">
        <section
          className={cx(styles.titleLarge, 'col-span-4', 'col-span-lg-12')}
          aria-label="Naam website"
        >
          <h1 id="homePageTitle" className="mb-32 mb-lg-64" aria-hidden>
            Onderzoek en Statistiek
          </h1>
        </section>

        {flatFeatures.length > 0 && largeFirstFeature && (
          <section
            className="col-start-1 col-start-lg-5 col-span-4 col-span-lg-8 mb-80"
            aria-label="Hoofditem"
          >
            <CardImage
              aspectRatio="16-9"
              image={flatFeatures[0].rectangularImage}
              sizes="(max-width: 840px) 840px, 920px"
              type={flatFeatures[0].type}
              fullWidth
              priority
            />
            <Card className="mb-40" variant="large">
              <CardHeadingGroup>
                <h2>
                  <Link data-inline href={flatFeatures[0].path}>
                    {flatFeatures[0].shortTitle || flatFeatures[0].title}
                  </Link>
                </h2>
                <p className="mt-20" data-small>
                  {CONTENT_TYPES[flatFeatures[0].type.toLowerCase()].name}
                </p>
              </CardHeadingGroup>
              <p>{flatFeatures[0].teaser}</p>
            </Card>
          </section>
        )}

        <section
          className="col-start-1 col-start-lg-5 col-span-4 col-span-lg-8"
          aria-labelledby="headingFeatured"
        >
          {largeFirstFeature && (
            <h2
              id="headingFeatured"
              className="mt-16 mt-lg-80 mb-24 mb-lg-32"
              data-style-as="h3"
            >
              Uitgelicht
            </h2>
          )}

          <ul className={styles.featureList}>
            {flatFeatures
              .slice(largeFirstFeature ? 1 : 0)
              .map(
                ({
                  path,
                  title,
                  shortTitle,
                  teaser,
                  rectangularImage,
                  type,
                }) => (
                  <li key={path}>
                    <Card className="mb-40" variant="medium">
                      <CardImage
                        image={rectangularImage}
                        type={type}
                        sizes="(max-width: 840px) 840px, 450px"
                      />
                      <CardHeadingGroup>
                        <h3 data-style-as="h5">
                          <Link data-inline href={path}>
                            {shortTitle || title}
                          </Link>
                        </h3>
                        <p data-small className="mt-20">
                          {CONTENT_TYPES[type.toLowerCase()].name}
                        </p>
                      </CardHeadingGroup>
                      <p data-small>{teaser}</p>
                    </Card>
                  </li>
                ),
              )}
          </ul>
        </section>

        <nav
          className={cx(
            styles.navLarge,
            'col-span-4',
            'col-span-lg-3',
            'row-span-6',
          )}
          aria-labelledby="headingSideBar"
        >
          <h2 className="hide-visually" id="headingSideBar">
            Menu
          </h2>
          <h3 className="mb-12" data-style-as="h5">
            Thema’s
          </h3>
          <ul className="mb-56">
            {themes
              .slice() // strict mode freezes arrays, so we need to make a copy to be able to sort
              .sort((a, b) => a.title.localeCompare(b.title))
              .map(({ title, slug }) => (
                <li className="mb-12" key={slug}>
                  <Link
                    href={`/thema/${slug}`}
                    data-variant="inList"
                    className="analytics-homepage-theme-link"
                  >
                    <Icon name="chevron-right" size="14px"></Icon>
                    {title}
                  </Link>
                </li>
              ))}
          </ul>
          {agendaList}
          {shortcuts.length > 0 && (
            <>
              <h3 className="mb-12" data-style-as="h5">
                Snel naar
              </h3>
              <ul className="mb-56">
                {normalizeItemList(shortcuts).map(
                  ({ path, title, shortTitle }) => (
                    <li className="mb-12" key={path}>
                      <Link
                        href={path}
                        data-variant="inList"
                        className="analytics-homepage-shortcut-link"
                      >
                        <Icon name="chevron-right" size="14px"></Icon>
                        {shortTitle || title}
                      </Link>
                    </li>
                  ),
                )}
              </ul>
            </>
          )}
          {relatedSiteList}
        </nav>
      </div>

      {featuredCollections.length > 0 && (
        <section className="grid mb-40" aria-labelledby="headingCollections">
          <section className="col-span-4 col-span-lg-12">
            <h2 id="headingCollections" className="mb-40">
              Dossiers
            </h2>
          </section>
          <ul className="grid col-span-4 col-span-lg-12">
            {normalizeItemList(featuredCollections).map(
              ({
                path,
                title: featureTitle,
                shortTitle: featureShortTitle,
                teaser: featureTeaser,
              }) => (
                <li className="col-span-4" key={path}>
                  <Card className="mb-40">
                    <h3 data-style-as="h5">
                      <Link data-inline href={path}>
                        {featureShortTitle || featureTitle}
                      </Link>
                    </h3>
                    <p data-small>{featureTeaser}</p>
                  </Card>
                </li>
              ),
            )}
          </ul>
          <section className="col-span-4 col-span-lg-12">
            <Link variant="standalone" href="/zoek?categorie=dossier">
              Bekijk alle dossiers
            </Link>
          </section>
        </section>
      )}

      <nav className={styles.nav}>
        <section className="col-span-4">{agendaList}</section>
        <section className="col-span-4">{relatedSiteList}</section>
      </nav>
    </div>
  )
}

export async function getStaticProps() {
  try {
    const homepage = await fetchAPI(
      `/api/homepage?${qs.stringify(homepageQuery, {
        encodeValuesOnly: true,
      })}`,
    )
    const themes = await fetchAPI(
      `/api/themes?${qs.stringify(themesQuery, { encodeValuesOnly: true })}`,
    )

    return {
      props: {
        themes: themes.data,
        homepage: homepage.data,
      },
      revalidate: 10,
    }
  } catch (error) {
    return {
      props: {},
      revalidate: 10,
    }
  }
}

export default Home
