import dynamic from 'next/dynamic'
import Image from 'next/image'
import { useRouter } from 'next/router'
import qs from 'qs'

import Seo from '../../components/Seo/Seo'
import FallbackPage from '../../components/FallbackPage/FallbackPage'
import ByLine from '../../components/ByLine/ByLine'
import Alert from '../../components/Alert/Alert'
import Link from '../../components/Link/Link'
import { Card, CardHeadingGroup, CardImage } from '../../components/Card/Card'
import ContentFooter from '../../components/ContentFooter/ContentFooter'
import {
  fetchAPI,
  getStrapiMedia,
  PLACEHOLDER_IMAGE,
  formatDate,
} from '../../lib/utils'
import { normalizeItemList } from '../../lib/normalizeUtils'
import { cx } from '../../lib/styleUtils'
import CONTENT_TYPES from '../../constants/contentTypes'
import { articleQuery } from '../../lib/queries'

import styles from './article.module.css'

const BodyContent = dynamic(
  () => import('../../components/BodyContent/BodyContent'),
)

const Article = ({
  title,
  shortTitle,
  teaser,
  rectangularImage,
  publicationDate,
  author,
  intro,
  body,
  themes,
  related,
  toc,
  showDate = true,
  publishedAt,
}) => {
  const router = useRouter()
  if (router.isFallback) {
    return <FallbackPage />
  }

  // do we want the date in the byline?
  const byline =
    showDate !== false ? [author, formatDate(publicationDate)] : [author]

  return (
    <>
      <Seo
        title={shortTitle || title}
        description={teaser || intro}
        image={getStrapiMedia(rectangularImage)}
        article
      />
      <div className={cx(styles.container, 'grid')}>
        <div className="col-start-1 col-start-lg-2 col-span-4 col-span-lg-10">
          {!publishedAt && <Alert />}
          <h1 className="mb-16">{title}</h1>
          <ByLine className={cx('mb-56', 'mb-lg-40')} items={byline} />
          <p className="mb-56 mb-lg-80" data-large>
            {intro}
          </p>

          {rectangularImage && (
            <div className={cx(styles.image, 'mb-56', 'mb-lg-80')}>
              <Image
                src={getStrapiMedia(rectangularImage)}
                alt=""
                width={rectangularImage.width}
                height={rectangularImage.height}
                placeholder="blur"
                blurDataURL={PLACEHOLDER_IMAGE}
                priority
                sizes="(max-width: 840px) 840px, 920px"
                style={{
                  width: '100%',
                  height: 'auto',
                }}
              />
              {rectangularImage.caption &&
                /*
                  Strapi autofills the 'caption' field with the name of the file.
                  This regex checks if 'caption' doesn't end in an image extension.
                */
                !/\.(jpg|jpeg|png|webp|avif|gif|svg|ico)$/.test(
                  rectangularImage.caption,
                ) && (
                  <p className="mt-8" data-small>
                    {rectangularImage.caption}
                  </p>
                )}
            </div>
          )}
        </div>

        {body && <BodyContent content={body} toc={toc} />}

        <div className="col-start-1 col-start-lg-3 col-span-4 col-span-lg-8">
          <ContentFooter type="artikel" themes={themes} />
        </div>

        {related.length > 0 && (
          <>
            <div className="col-span-4 col-span-g-12">
              <h2 data-style-as="h4" className="mb-40">
                Ook interessant
              </h2>
            </div>
            <ul className="grid col-span-4 col-span-lg-12">
              {normalizeItemList(related).map(
                ({
                  path,
                  title: relatedTitle,
                  shortTitle: relatedShortTitle,
                  rectangularImage: relatedRectangularImage,
                  type,
                }) => (
                  <li key={path} className="mb-40 col-span-4">
                    <Card variant="medium">
                      <CardImage
                        image={relatedRectangularImage}
                        type={type}
                        sizes="(max-width: 840px) 840px, 450px"
                      />
                      <CardHeadingGroup>
                        <h3 href={path} data-style-as="h5">
                          <Link data-inline href={path}>
                            {relatedShortTitle || relatedTitle}
                          </Link>
                        </h3>
                        <p data-small className="mt-20">
                          {CONTENT_TYPES[type.toLowerCase()].name}
                        </p>
                      </CardHeadingGroup>
                    </Card>
                  </li>
                ),
              )}
            </ul>
          </>
        )}
      </div>
    </>
  )
}

export async function getStaticPaths() {
  const articles = await fetchAPI('/api/articles?fields=id').then(
    (metaResult) =>
      fetchAPI(
        `/api/articles?fields=slug&pagination[pageSize]=${metaResult.meta.pagination.total}`,
      ),
  )

  return {
    paths: articles.data.map(({ slug }) => ({
      params: {
        slug,
      },
    })),
    fallback: true,
  }
}

export async function getStaticProps({ params, preview }) {
  const url = `/api/articles?${qs.stringify(
    {
      filters: { slug: { $eq: params.slug } },
      ...articleQuery,
    },
    {
      encodeValuesOnly: true,
    },
  )}${preview ? '&status=draft' : ''}`

  const data = await fetchAPI(url)

  if (!data.data[0]) {
    return {
      notFound: true,
      revalidate: 10,
    }
  }

  return {
    props: data.data[0],
    revalidate: 10,
  }
}

export default Article
