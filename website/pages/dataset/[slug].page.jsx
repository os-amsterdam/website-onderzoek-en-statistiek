import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import qs from 'qs'

import FallbackPage from '../../components/FallbackPage/FallbackPage'
import Seo from '../../components/Seo/Seo'
import Alert from '../../components/Alert/Alert'
import Link from '../../components/Link/Link'
import DownloadButton from '../../components/DownloadButton/DownloadButton'
import ContentFooter from '../../components/ContentFooter/ContentFooter'
import {
  fetchAPI,
  getStrapiMedia,
  formatDate,
  formatBytes,
  mapOrder,
} from '../../lib/utils'
import { cx } from '../../lib/styleUtils'

import { datasetQuery } from '../../lib/queries'
import styles from './dataset.module.css'

const BodyContent = dynamic(
  () => import('../../components/BodyContent/BodyContent'),
)

const Dataset = ({
  title,
  teaser,
  body,
  contactName,
  contactMail,
  purpose,
  publishedAt: published,
  updatedAt: updated,
  frequency,
  resources,
  rectangularImage,
  themes,
  publishedAt,
}) => {
  const router = useRouter()
  if (router.isFallback) {
    return <FallbackPage />
  }

  const order = ['documentatie', 'databestand', 'api', 'visualisatie']
  const orderedResources = mapOrder(resources, order, 'type')

  return (
    <>
      <Seo
        title={`Dataset: ${title}`}
        description={
          teaser ||
          body.reduce((allText, bodyItem) => allText + bodyItem.text, '')
        }
        image={getStrapiMedia(rectangularImage)}
        article
      />

      <div className={cx(styles.container, 'grid')}>
        <div className="col-start-1 col-start-lg-2 col-span-4 col-span-lg-10">
          {!publishedAt && <Alert />}
          <h1 className="mb-24">{`Dataset ${title}`}</h1>
        </div>

        {body && (
          <BodyContent
            content={body}
            colStartText={{ small: 1, large: 2 }}
            colRangeText={{ small: 4, large: 10 }}
            colStartTextWithLink={{ small: 1, large: 2 }}
            colRangeTextWithLink={{ small: 4, large: 7 }}
          />
        )}

        <section
          className="col-start-1 col-start-lg-2 col-span-4 col-span-lg-10 mb-40 mb-lg-80"
          aria-labelledby="headingSources"
        >
          <h2 id="headingSources" className="mb-20 mb-lg-40 mt-40">
            Bronnen
          </h2>
          <table className={styles.table}>
            <thead>
              <tr>
                <th>Omschrijving</th>
                <th>Categorie</th>
                <td />
              </tr>
            </thead>
            <tbody>
              {orderedResources.map(
                ({ id, title: resourceTitle, file, url, type }) =>
                  (file || url) && (
                    <tr key={id}>
                      <td>
                        {file ? (
                          resourceTitle
                        ) : (
                          <Link href={url} data-small data-inline>
                            {resourceTitle}
                          </Link>
                        )}
                      </td>
                      <td>{type}</td>
                      {file ? (
                        <td>
                          <DownloadButton
                            url={getStrapiMedia(file)}
                            type={type}
                            data-as-link
                            data-small
                            data-inlist
                          >
                            {`Download ${
                              file && file.ext.slice(1).toUpperCase()
                            } ${
                              file?.size && `(${formatBytes(file.size * 1000)})`
                            }`}
                          </DownloadButton>
                        </td>
                      ) : (
                        <td />
                      )}
                    </tr>
                  ),
              )}
            </tbody>
          </table>
        </section>

        <section
          className="col-start-1 col-start-lg-2 col-span-4 col-span-lg-10 mb-40 mb-lg-80"
          aria-labelledby="headingMetaData"
        >
          <h2 className="mb-20 mb-lg-40" id="headingMetaData">
            Beschrijving
          </h2>
          <dl className={styles.list}>
            <div>
              <dt>Publicatiedatum</dt>
              <dd>{formatDate(published)}</dd>
            </div>
            <div>
              <dt>Wijzigingsdatum</dt>
              <dd>{formatDate(updated)}</dd>
            </div>
            <div>
              <dt>Wijzigingsfrequentie</dt>
              <dd>{frequency}</dd>
            </div>
            <div>
              <dt>Doel</dt>
              <dd>{purpose}</dd>
            </div>
            <div>
              <dt>Contactpersoon</dt>
              <dd>
                <Link
                  href={`mailto:${contactMail}`}
                  data-small
                  data-inline
                  external
                >
                  {contactName}
                </Link>
              </dd>
            </div>
          </dl>
        </section>

        <div className="col-start-1 col-start-lg-2 col-span-4 col-span-lg-10">
          <ContentFooter type="dataset" themes={themes} />
        </div>
      </div>
    </>
  )
}

export async function getStaticPaths() {
  const datasets = await fetchAPI('/api/datasets?fields=id').then(
    (metaResult) =>
      fetchAPI(
        `/api/datasets?fields=slug&pagination[pageSize]=${metaResult.meta.pagination.total}`,
      ),
  )

  return {
    paths: datasets.data.map(({ slug }) => ({
      params: {
        slug,
      },
    })),
    fallback: true,
  }
}

export async function getStaticProps({ params, preview }) {
  const data = await fetchAPI(
    `/api/datasets?${qs.stringify(
      {
        filters: { slug: { $eq: params.slug } },
        ...datasetQuery,
      },
      {
        encodeValuesOnly: true,
      },
    )}${preview ? '&status=draft' : ''}`,
  )

  if (!data.data[0]) {
    return {
      notFound: true,
      revalidate: 10,
    }
  }

  return {
    props: data.data[0],
    revalidate: 10,
  }
}

export default Dataset
