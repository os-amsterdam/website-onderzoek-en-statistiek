import Document, { Html, Head, Main, NextScript } from 'next/document'

export default class MyDocument extends Document {
  render() {
    return (
      <Html lang="nl">
        <Head>
          <link rel="icon" href="/favicon.ico" />
          <link
            rel="stylesheet"
            crossOrigin="anonymous"
            href="https://onderzoek.amsterdam.nl/static/style/index.css"
          />
          <script
            type="module"
            src="https://onderzoek.amsterdam.nl/static/style/os-icon.js"
            async
          ></script>
          <script
            src="https://onderzoek.amsterdam.nl/static/style/os-icon.js"
            type="module"
            defer
          />
          <script
            type="module"
            defer
            src="https://onderzoek.amsterdam.nl/static/style/os-pagination.js"
          />

          <link
            rel="stylesheet"
            href="https://onderzoek.amsterdam.nl/static/datavisualisatie-onderzoek-en-statistiek/os-visualisation-style.css"
            crossOrigin="anonymous"
          />
          <script
            type="module"
            src="https://onderzoek.amsterdam.nl/static/datavisualisatie-onderzoek-en-statistiek/os-visualisation.js"
            async
          ></script>

          <meta
            name="google-site-verification"
            content="rK12dI2cqNYBT7VebPATLF6NyMAuQsocYjHVvpTfTcI"
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}
