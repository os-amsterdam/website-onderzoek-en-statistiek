import App from 'next/app'
import { useRouter } from 'next/router'
import Script from 'next/script'
import { useState, useEffect } from 'react'
import Fuse from 'fuse.js'
import qs from 'qs'

import Layout from '../components/Layout/Layout'
import { prependRootURL, fetchAPI } from '../lib/utils'
import { startTracking, pushCustomEvent } from '../lib/analyticsUtils'
import ShortcutContext from '../lib/ShortcutContext'
import { fuseOptions, SearchContext } from '../lib/searchUtils'

import '../styles/global.css'

const MyApp = ({ Component, pageProps, contextData }) => {
  const [searchIndex, setSearchIndex] = useState(null)
  const router = useRouter()
  const { query, asPath } = router

  if (query.slug && query.slug !== query.slug.toLowerCase()) {
    pushCustomEvent('Redirect', 'Slug with uppercase letters', asPath)
    router.push({
      pathname: '[slug]',
      query: {
        ...query,
        slug: query.slug.toLowerCase(),
      },
    })
  }

  useEffect(() => {
    const abortController = new AbortController()

    fetch(prependRootURL('/api/search-content'), {
      signal: abortController.signal,
      mode: 'cors',
    })
      .then((response) => response.json())
      .then((searchContent) => {
        // restructure data from search content api for Fuse here
        const restructuredSearchContent = searchContent.map((item) => {
          const { title, teaser, additionalTerms, keywords, resources } = item

          return {
            ...item,
            titleWords: title,
            allSearchFields: `${title} ${teaser || ''} ${
              additionalTerms || ''
            }  ${keywords || ''} ${resources}`.trim(),
          }
        })

        setSearchIndex(new Fuse(restructuredSearchContent, fuseOptions))
      })
      .catch() // TODO: log errors in Sentry
    return () => abortController.abort()
  }, [])

  useEffect(() => {
    startTracking(router)
  }, [])

  return (
    <SearchContext.Provider value={searchIndex}>
      <ShortcutContext.Provider value={contextData.data.shortcuts}>
        <Layout>
          <Script id="piwik-pro-code" src="/piwik.js" />
          <Component {...pageProps} />
        </Layout>
      </ShortcutContext.Provider>
    </SearchContext.Provider>
  )
}

MyApp.getInitialProps = async (appContext) => {
  const appProps = await App.getInitialProps(appContext)

  const appQuery = {
    populate: '*',
  }
  const { data } = await fetchAPI(
    `/api/homepage?${qs.stringify(appQuery, {
      encodeValuesOnly: true,
    })}`,
  )

  return { ...appProps, contextData: { data } }
}

export default MyApp
