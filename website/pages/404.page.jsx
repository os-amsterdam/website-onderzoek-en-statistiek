import { useRouter } from 'next/router'

import Seo from '../components/Seo/Seo'
import Link from '../components/Link/Link'
import CONTENT_TYPES from '../constants/contentTypes'
import { cx } from '../lib/styleUtils'

import styles from './homepage.module.css'

const searchLinkLogic = (path) => {
  const segments = path.substring(1).split(/-|\//) // split path on forward slashes and minus signs
  const contentType = Object.values(CONTENT_TYPES).find(
    (item) => item.name === segments[0],
  )
  const isTheme = segments[0] === 'thema'

  if (isTheme) {
    return {
      type: null,
      text: segments.slice(1).join(' '),
      path: `/zoek?tekst=${segments.slice(1).join('+')}`,
    }
  }

  if (contentType) {
    return {
      type:
        contentType.plural !== 'Interactief'
          ? contentType.plural.toLowerCase()
          : 'interactieve publicaties',
      text: segments.slice(1).join(' '),
      path: `/zoek?categorie=${segments[0]}&tekst=${segments
        .slice(1)
        .join('+')}`,
    }
  }

  return {
    type: null,
    text: segments.join(' '),
    path: `/zoek?tekst=${segments.join('+')}`,
  }
}

const Custom404 = () => {
  const router = useRouter()
  const searchLinkObject = searchLinkLogic(router.asPath)

  return (
    <>
      <Seo title="Pagina niet gevonden" />
      <div className={cx(styles.container, styles.container404, 'grid')}>
        <div className="col-start-1 col-start-lg-3 col-span-4 col-span-lg-8">
          <h1 className="mb-56 mb-lg-40">Helaas</h1>
          <p className="mb-40">
            Deze pagina bestaat niet (meer). Controleer het adres of:
          </p>
          <ul data-ul className="mb-40" data-small>
            <li>
              Zoek naar{' '}
              <Link href={searchLinkObject.path} data-inline>
                {`${
                  searchLinkObject.type
                    ? `${searchLinkObject.type} met de tekst`
                    : ''
                } '${searchLinkObject.text}'`}
              </Link>
            </li>
            <li>
              Ga naar de{' '}
              <Link href="/" data-inline>
                voorpagina
              </Link>
            </li>
            <li>
              Stuur een{' '}
              <Link href="mailto:redactie.os@amsterdam.nl" data-inline external>
                bericht aan de redactie
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </>
  )
}

export default Custom404
