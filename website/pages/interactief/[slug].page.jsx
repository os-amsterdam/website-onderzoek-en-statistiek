import { useEffect } from 'react'
import { useRouter } from 'next/router'
import qs from 'qs'

import FallbackPage from '../../components/FallbackPage/FallbackPage'
import { fetchAPI, getStrapiMedia } from '../../lib/utils'
import Seo from '../../components/Seo/Seo'
import Alert from '../../components/Alert/Alert'
import IFrame from '../../components/IFrame/IFrame'
import ContentFooter from '../../components/ContentFooter/ContentFooter'
import { interactiveQuery } from '../../lib/queries'

import styles from './interactief.module.css'

const Interactive = ({
  title,
  shortTitle,
  teaser,
  implementation,
  contentLink,
  rectangularImage,
  themes,
  publishedAt,
}) => {
  const router = useRouter()

  useEffect(() => {
    if (implementation === 'link') {
      router.push(contentLink)
    }
  }, [implementation])

  if (router.isFallback) {
    return <FallbackPage />
  }

  return (
    <div className={styles.style}>
      <Seo
        title={shortTitle || title}
        description={teaser}
        image={getStrapiMedia(rectangularImage)}
        article
      />
      {!publishedAt && <Alert />}
      <h1 className="hide-visually">{title}</h1>
      <IFrame src={contentLink} title={title} />
      <div className="grid">
        <div className="col-span-4 col-span-lg-8">
          <ContentFooter type="interactieve publicatie" themes={themes} />
        </div>
      </div>
    </div>
  )
}

export async function getStaticPaths() {
  const interactives = await fetchAPI('/api/interactives?fields=id').then(
    (metaResult) =>
      fetchAPI(
        `/api/interactives?fields=slug&pagination[pageSize]=${metaResult.meta.pagination.total}`,
      ),
  )

  return {
    paths: interactives.data.map(({ slug }) => ({
      params: {
        slug,
      },
    })),
    fallback: true,
  }
}

export async function getStaticProps({ params, preview }) {
  const data = await fetchAPI(
    `/api/interactives?${qs.stringify(
      {
        filters: { slug: { $eq: params.slug } },
        ...interactiveQuery,
      },
      {
        encodeValuesOnly: true,
      },
    )}${preview ? '&status=draft' : ''}`,
  )

  if (!data.data[0]) {
    return {
      notFound: true,
      revalidate: 10,
    }
  }

  return {
    props: data.data[0],
    revalidate: 10,
  }
}

export default Interactive
