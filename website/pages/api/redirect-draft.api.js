import qs from 'qs'

import CONTENT_TYPES from '../../constants/contentTypes'
import { fetchAPI } from '../../lib/utils'

export default async function handler(req, res) {
  if (!req.query.slug) {
    return res.status(401).json({
      message:
        'Onjuiste url, de slug ontbreekt. Neem contact op met redactie.os@amsterdam.nl voor vragen.',
    })
  }

  // check if item exists
  const url = `/api/${req.query.type}s?${qs.stringify(
    {
      filters: {
        slug: { $eq: req.query.slug },
      },
      fields: ['slug'],
      status: 'draft',
    },
    { encodeValuesOnly: true },
  )}`

  const pageData = await fetchAPI(url)

  if (!pageData.data?.length > 0) {
    return res.status(401).json({
      message:
        'Onjuiste url, deze slug bestaat niet. Neem contact op met redactie.os@amsterdam.nl voor vragen.',
    })
  }

  const path = `/${CONTENT_TYPES[req.query.type].name}/${pageData.data[0].slug}`

  // preview cookie expires in an hour and is only valid for this specific page
  res.setPreviewData(
    {},
    {
      maxAge: 60 * 60,
      path,
    },
  )

  res.writeHead(307, {
    Location: path,
  })
  res.end()
}
