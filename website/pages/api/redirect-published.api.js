import CONTENT_TYPES from '../../constants/contentTypes'

export default function handler(req, res) {
  try {
    const { type, slug } = req.query
    const path = `/${CONTENT_TYPES[type].name}/${slug}`

    // Clears the preview mode cookies.
    res.clearPreviewData({ path })

    // redirects to published page
    res.writeHead(307, {
      Location: path,
    })
    res.end()
  } catch (error) {
    return res.status(500).json({ message: error })
  }
}
