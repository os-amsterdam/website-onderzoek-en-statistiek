const findDuplicates = (arr, key) => {
  const allKeys = []
  const duplicates = []
  arr.forEach((d) => {
    if (allKeys.includes(d[key])) duplicates.push(d)
    allKeys.push(d[key])
  })
  return duplicates
}

const checkForDuplicates = (baseUrl, verbose = false) => {
  console.log(`Checking for duplicate resources on ${baseUrl}`)
  const url = `${baseUrl}api/datasets?pagination[pageSize]=200&populate[resources][populate][0]=file`
  fetch(url)
    .then((result) => result.json())
    .then((json) => {
      const datasetsWithDuplicates = json.data.filter((d) => {
        const duplicates = findDuplicates(d.resources, 'id')
        if (duplicates.length > 0) {
          console.warn(
            `Found ${duplicates.length} duplicate resources found in dataset ${d.title}`,
          )
          if (verbose) {
            console.table(
              duplicates.map(({ id, title, file }) => ({
                id,
                title: `${title.substr(0, 50)}...`,
                file: file?.name,
              })),
            )
          }

          return true
        }
        return false
      })
      console.log(
        `Found ${datasetsWithDuplicates.length} datasets with duplicates on ${baseUrl}`,
      )
    })
    .catch((err) => console.log(err))
}

const [arg1, arg2] = process.argv.slice(2)
let prefix = ''
if (arg1 === 'acc') prefix = 'acc.'
const baseUrl = arg1
  ? `https://${prefix}cms.onderzoek-en-statistiek.nl/`
  : 'http://localhost:1337/'
const verbose = arg2 === 'verbose'

checkForDuplicates(baseUrl, verbose)
