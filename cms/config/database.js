module.exports = ({ env }) => ({
  connection: {
    client: 'postgres',
    connection: {
      host: env('STRAPI_DB_HOST', '127.0.0.1'),
      port: env.int('STRAPI_DB_PORT', 5432),
      database: env('STRAPI_DB_NAME', 'db-name-for-local-development'),
      user: env('STRAPI_DB_USERNAME', 'db-username-for-local-development'),
      password: env('STRAPI_DB_PASSWORD', 'db-password-for-local-development'),
      schema: env('STRAPI_DB_SCHEMA', 'public'),
    },
    debug: false,
  },
})
