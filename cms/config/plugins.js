module.exports = ({ env }) => ({
  upload: {
    enabled: true,
    config: {
      providerOptions: {
        localServer: {
          maxage: 604800 * 1000, // cashing for image in the media library,  604800 = week, 31536000 = year
        },
      },
    },
  },
  'preview-button': {
    config: {
      contentTypes: [
        {
          uid: 'api::homepage.homepage',
          published: {
            url: env('STRAPI_FE_WEBSITE_DOMAIN'),
          },
        },
        {
          uid: 'api::article.article',
          published: {
            url: `${env('STRAPI_FE_WEBSITE_DOMAIN')}/api/redirect-published`,
            query: {
              type: 'article',
              slug: '{slug}',
            },
          },
          draft: {
            url: `${env('STRAPI_FE_WEBSITE_DOMAIN')}/api/redirect-draft`,
            query: {
              type: 'article',
              slug: '{slug}',
            },
          },
        },
        {
          uid: 'api::collection.collection',
          published: {
            url: `${env('STRAPI_FE_WEBSITE_DOMAIN')}/api/redirect-published`,
            query: {
              type: 'collection',
              slug: '{slug}',
            },
          },
          draft: {
            url: `${env('STRAPI_FE_WEBSITE_DOMAIN')}/api/redirect-draft`,
            query: {
              type: 'collection',
              slug: '{slug}',
            },
          },
        },
        {
          uid: 'api::dataset.dataset',
          published: {
            url: `${env('STRAPI_FE_WEBSITE_DOMAIN')}/api/redirect-published`,
            query: {
              type: 'dataset',
              slug: '{slug}',
            },
          },
          draft: {
            url: `${env('STRAPI_FE_WEBSITE_DOMAIN')}/api/redirect-draft`,
            query: {
              type: 'dataset',
              slug: '{slug}',
            },
          },
        },
        {
          uid: 'api::interactive.interactive',
          published: {
            url: `${env('STRAPI_FE_WEBSITE_DOMAIN')}/api/redirect-published`,
            query: {
              type: 'interactive',
              slug: '{slug}',
            },
          },
          draft: {
            url: `${env('STRAPI_FE_WEBSITE_DOMAIN')}/api/redirect-draft`,
            query: {
              type: 'interactive',
              slug: '{slug}',
            },
          },
        },
        {
          uid: 'api::publication.publication',
          published: {
            url: `${env('STRAPI_FE_WEBSITE_DOMAIN')}/api/redirect-published`,
            query: {
              type: 'publication',
              slug: '{slug}',
            },
          },
          draft: {
            url: `${env('STRAPI_FE_WEBSITE_DOMAIN')}/api/redirect-draft`,
            query: {
              type: 'publication',
              slug: '{slug}',
            },
          },
        },
        {
          uid: 'api::theme.theme',
          published: {
            url: `${env('STRAPI_FE_WEBSITE_DOMAIN')}/api/redirect-published`,
            query: {
              type: 'theme',
              slug: '{slug}',
            },
          },
          draft: {
            url: `${env('STRAPI_FE_WEBSITE_DOMAIN')}/api/redirect-draft`,
            query: {
              type: 'theme',
              slug: '{slug}',
            },
          },
        },
        {
          uid: 'api::video.video',
          published: {
            url: `${env('STRAPI_FE_WEBSITE_DOMAIN')}/api/redirect-published`,
            query: {
              type: 'video',
              slug: '{slug}',
            },
          },
          draft: {
            url: `${env('STRAPI_FE_WEBSITE_DOMAIN')}/api/redirect-draft`,
            query: {
              type: 'video',
              slug: '{slug}',
            },
          },
        },
      ],
    },
  },
})
