module.exports = ({ env }) => ({
  apiToken: {
    salt: env('STRAPI_API_TOKEN_SALT', 'api-token-salt-for-local-development'),
  },
  auth: {
    secret: env(
      'STRAPI_FE_JWT_SECRET',
      'strapi-fe-jwt-secret-for-local-development',
    ),
  },
  transfer: {
    token: {
      salt: env(
        'STRAPI_CLI_TRANSFER_TOKEN_SALT',
        'transfer-token-salt-for-local-development',
      ),
    },
  },
})
