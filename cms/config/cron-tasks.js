// based on these docs: https://forum.strapi.io/t/schedule-publications/23184

function fetchItems(endpoint) {
  return strapi.documents(endpoint).findMany({
    filters: {
      $and: [
        {
          publish_at: {
            $notNull: true, // filter for draft items with a datetime that is not null...
          },
          publish_at: {
            $lt: new Date(), // AND a value that is lower than the current datetime
          },
        },
      ],
    },
  })
}

function publishItems(items, endpoint) {
  return items.map((item) =>
    strapi.documents(endpoint).update({
      documentId: item.documentId,
      status: 'published',
      data: {
        publish_at: null,
      },
    }),
  )
}

module.exports = {
  /**
   * Scheduled publication workflow.
   * Checks every minute if there are draft items to publish.
   */

  '*/1 * * * *': async () => {
    const endpoints = [
      'api::article.article',
      'api::collection.collection',
      'api::dataset.dataset',
      'api::interactive.interactive',
      'api::publication.publication',
      'api::video.video',
    ]

    // Fetch items to publish
    const fetchPromises = endpoints.map(fetchItems)
    const draftItems = await Promise.all(fetchPromises)

    // Publish items and remove scheduled publish date
    const publishPromises = draftItems.flatMap((items, index) =>
      publishItems(items, endpoints[index]),
    )
    await Promise.all(publishPromises)
  },
}
