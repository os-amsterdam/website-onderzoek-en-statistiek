const cronTasks = require('./cron-tasks')

module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  app: {
    keys: env.array('STRAPI_FE_APP_KEYS', ['app-key-for-local-development']),
  },
  cron: {
    enabled: true,
    tasks: cronTasks,
  },
  logger: {
    updates: {
      enabled: false, // disable update notification logging
    },
  },
})
