#!/bin/bash

# Load .env file
if [ -f .env ]; then
  export $(grep -v '^#' .env | xargs)
else
  echo ".env file not found. Please create one."
  exit 1
fi

# Determine the environment (default to 'prod')
ENVIRONMENT="${1:-prod}" # First argument, default to 'prod'

# Validate the environment and set the container name
if [[ "$ENVIRONMENT" == "acc" ]]; then
  DB_CONTAINER_NAME="os-db-acc"
  CMS_CONTAINER_NAME="os-cms-acc"
elif [[ "$ENVIRONMENT" == "prod" ]]; then
  DB_CONTAINER_NAME="os-db-prod"
  CMS_CONTAINER_NAME="os-cms-prod"
else
  echo "Use 'acc' or 'prod',"
  exit 1
fi

# Config
LOCAL_BACKUP_DIR="./backup"
TIMESTAMP=$(date +"%Y%m%d_%H%M%S")
DB_BACKUP_FILE="backup_database_${ENVIRONMENT}_${TIMESTAMP}.sql.gz"
CMS_BACKUP_FILE="backup_cms_${ENVIRONMENT}_${TIMESTAMP}"

# create database backup with pg_dump, including user data
echo "Creating, compressing, and downloading a backup for the ${ENVIRONMENT} database..."
ssh $SSH_HOST \
  "docker exec $DB_CONTAINER_NAME sh -c 'pg_dump -U $DB_USER $DB_NAME' | gzip" > "$LOCAL_BACKUP_DIR/$DB_BACKUP_FILE"

# Create strapi backup inside the container
echo "Creating, compressing, and downloading a backup for the ${ENVIRONMENT} cms..."
ssh $SSH_HOST \
  "docker exec $CMS_CONTAINER_NAME npm run strapi export -- --no-encrypt -f /tmp/$CMS_BACKUP_FILE"

# Copy the strapi backup file from the container to local machine
ssh $SSH_HOST \
  "docker cp $CMS_CONTAINER_NAME:/tmp/$CMS_BACKUP_FILE.tar.gz -" > "$LOCAL_BACKUP_DIR/$CMS_BACKUP_FILE.tar.gz"

# Clean up the strapi backup file from inside the container
ssh $SSH_HOST \
  "docker exec $CMS_CONTAINER_NAME rm -f /tmp/$CMS_BACKUP_FILE.tar.gz"

# Check if the local backup was successful
if [ $? -eq 0 ]; then
  echo "Backup completed successfully"
else
  echo "Error occurred during backup process."
fi