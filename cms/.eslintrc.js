module.exports = {
  env: {
    commonjs: true,
    node: true,
    browser: true,
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  globals: {
    strapi: true,
  },
  rules: {
    semi: ['error', 'never'],
    'no-console': 'off',
    strict: 'off',
  },
}
