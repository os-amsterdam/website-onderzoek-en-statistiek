/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-console */
/* eslint-disable no-continue */
/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
import { createWriteStream } from 'fs'
import * as cheerio from 'cheerio'

const options = {
  headers: new Headers({
    Authorization: 'Basic ' + btoa('OenS:@Weesper113'),
    'Content-Type': 'application/x-www-form-urlencoded',
  }),
}

const getPagesFromSitemap = async (sitemapUrl, env) => {
  const result = await fetch(sitemapUrl, env === 'acc' ? options : undefined)

  if (result.status < 200 || result.status >= 300) {
    console.log('Fetch failed, you probably need to connect through VPN')
  }

  const xml = await result.text()
  const $ = cheerio.load(xml)
  const pages = $('loc')
    .map((_, value) => $(value).text())
    .toArray()
  return pages
}

const getLinksFromPage = async (pageUrl, env) => {
  const result = await fetch(pageUrl, env === 'acc' ? options : undefined)
  const html = await result.text()
  const $ = cheerio.load(html)
  const links = $('a')
    .map((_, value) => $(value).attr('href'))
    .toArray()
  return links
}

const checkLinks = async (env) => {
  const sitemapUrl =
    env === 'prod'
      ? 'https://onderzoek.amsterdam.nl/sitemap.xml'
      : 'https://acc.onderzoek.amsterdam.nl/sitemap.xml'

  const today = new Date().toISOString().split('T')[0]
  const fileName = `./broken-links-${today}.csv`
  const fileWriter = createWriteStream(fileName)
  fileWriter.write('page;link;status\n')

  const { protocol, hostname, port } = new URL(sitemapUrl)
  const baseUrl = port
    ? `${protocol}//${hostname}:${port}/`
    : `${protocol}//${hostname}/`
  const pages = await getPagesFromSitemap(sitemapUrl, env)
  const allLinks = []
  for (const page of pages) {
    const links = await getLinksFromPage(page, env)
    for (const link of links) {
      if (
        allLinks.includes(link) ||
        link.startsWith('mailto:') ||
        link.startsWith('tel:') ||
        link.startsWith('#')
      )
        continue
      const url = link.startsWith('http') ? link : baseUrl + link
      allLinks.push(link)

      try {
        const { status } = await fetch(url, env === 'acc' ? options : undefined)
        if (status < 200 || status >= 300) {
          fileWriter.write(`${page};${link};${status}\n`)
          console.log(page, link, status)
        }
      } catch (error) {
        fileWriter.write(`${page};${link};${error}\n`)
        console.log(page, link, error)
      }
    }
  }
}

const [env = 'prod'] = process.argv.slice(2)
checkLinks(env)
