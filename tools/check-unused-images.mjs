import fs from 'fs'

const getUrl = (type, prefix) =>
  `https://${prefix}cms.onderzoek-en-statistiek.nl/api/${type}?publicationState=preview&populate[visualisation][populate][image][fields][0]=url&populate[combiPicture][populate][biggerImage][populate]=url&populate[combiPicture][populate][smallerImage][populate]=url&populate[coverImage][fields][0]=url&populate[body][populate][panel][populate]=%2A&populate[body][populate][image][fields][0]=url&populate[squareImage][fields][0]=url&populate[rectangularImage][fields][0]=url&pagination[pageSize]=3000`

const extractInlineImage = (string) => {
  const regex = /(?<=\]\(\/uploads)(.*?)(\))/g
  const urls = string?.match(regex)

  return (
    urls?.length > 0 && urls.map((item) => `/uploads${item.replace(')', '')}`)
  )
}

const getUsedImageNames = async (env) => {
  const prefix = env === 'prod' ? '' : 'acc.'

  const contentTypes = [
    'articles',
    'publications',
    'videos',
    'interactives',
    'datasets',
    'collections',
    'themes',
  ]

  const urls = contentTypes.map((item) => getUrl(item, prefix))

  const imageNames = await Promise.all(
    urls.map(async (url) => {
      const resp = await fetch(url)
      const json = await resp.json()
      const sqImages = json.data.map((item) => item.squareImage?.url)
      const rectImages = json.data.map((item) => item.rectangularImage?.url)
      const coverImages = json.data.map((item) => item.coverImage?.url)
      const imagesInVisualisations = json.data
        .map((item) => item.body?.map((bodyItem) => bodyItem.image?.url))
        .flat()
      const imagesInPanelGroups = json.data
        .map((item) =>
          item.body?.map((bodyItem) =>
            bodyItem.panel?.map((panelItem) => panelItem.image?.url),
          ),
        )
        .flat(2)
      // get inline images from text field in text, textWithLinks and visualisation components
      const inlineImages = json.data
        .map((item) =>
          item.body?.map((bodyItem) => extractInlineImage(bodyItem.text)),
        )
        .flat(2)
      // get inline images from video transcript field
      const inlineImages2 = json.data
        .map((item) => extractInlineImage(item.transcript))
        .flat()
      // get inline images from panel text fields
      const inlineImages3 = json.data
        .map((item) =>
          item.body?.map((bodyItem) =>
            bodyItem.panel?.map((panelItem) =>
              extractInlineImage(panelItem.text),
            ),
          ),
        )
        .flat(2)

      const themeImages = json.data
        .map((item) => [
          item.combiPicture?.biggerImage.url,
          item.combiPicture?.smallerImage.url,
          item.visualisation?.image?.url,
        ])
        .flat()

      return [
        ...sqImages,
        ...rectImages,
        ...imagesInVisualisations,
        ...imagesInPanelGroups,
        ...inlineImages,
        ...inlineImages2,
        ...inlineImages3,
        ...coverImages,
        ...themeImages,
      ]
    }),
  )

  const uniqueImageNames = [...new Set(imageNames.flat())]

  return uniqueImageNames
}

const getAllImageNames = async (env) => {
  const prefix = env === 'prod' ? '' : 'acc.'

  const resp = await fetch(
    `https://${prefix}cms.onderzoek-en-statistiek.nl/api/upload/files?filters[$and][0][mime][$contains]=image`,
  )
  const json = await resp.json()

  return json.map((item) => item.url)
}

function arrayToCSV(data) {
  let csv = data.map((row) => row)
  return csv.join('\n')
}

const writeToFile = async () => {
  const [env = 'prod'] = process.argv.slice(2)

  const allImages = await getAllImageNames(env)
  const usedImages = await getUsedImageNames(env)

  const unUsedImages = allImages.filter((n) => !usedImages.includes(n))

  const csv = arrayToCSV(unUsedImages)

  fs.writeFileSync(`./unused-images.csv`, csv)
}

writeToFile()
