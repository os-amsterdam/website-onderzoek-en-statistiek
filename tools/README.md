# Tools

The scripts in this folder help to improve the health of our website

## Check for broken links

- `npm run check-broken-links`

The results are logged in a csv file in this folder: `broken-links-[date].csv`

The default script checks links on production, to check acceptance use `npm run check-broken-links acc`

## Check for broken images

- `npm run check-broken-images`

The results are logged in a csv file in this folder: `broken-images-[date].csv`

The default script checks images on production, to check acceptance use `npm run check-broken-images acc`

## Test website with Lighthouse

The script in this folder uses [Lighthouse](https://github.com/GoogleChrome/lighthouse) to test (a selection of) website pages. The results are saved as a csv file.

Scores are calculated for:

- Perfomance
- Accessibility
- SEO
- Best practices

### Prerequisites

- Google Chrome

### Usage

- `npm run lighthouse [environment] [scope] [logLevel]`

### Examples

- `npm run lighthouse`, calculates scores for a selection of pages on acceptance
- `npm run lighthouse acc all`, calculates scores for all pages on acceptance
- `npm run lighthouse prod`, calculates scores for a selection of pages on production
- `npm run lighthouse prod all`, calculates scores for all pages on production
- `npm run lighthouse acc article`, calculates scores for all articles on acceptance
- `npm run lighthouse acc collection`, calculates scores for all collections on acceptance
- `npm run lighthouse prod videos verbose`, calculates scores for all collections on production and shows result in console

## Check for unused images in media library

- `npm run check-unused-images`

This script compares all images used in the CMS (in published and draft items, in rectangularImage, inline, in visualisations, etc.) to all images in the media library. The difference should be images we don't use and can safely be deleted.

The default script checks images on production, to check acceptance use `npm run check-unused-images acc`
