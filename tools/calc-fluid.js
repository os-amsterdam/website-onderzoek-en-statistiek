const calculateFluidStyle = (
  minSizePx,
  maxSizePx,
  minScreenWidthPx = 320,
  maxScreenWidthPx = 1920,
) => {
  const defaultBaseSize = 16
  const minSize = minSizePx / defaultBaseSize
  const maxSize = maxSizePx / defaultBaseSize
  const minScreenWidth = minScreenWidthPx / defaultBaseSize
  const maxScreenWidth = maxScreenWidthPx / defaultBaseSize

  const css = `--fluid-size-${minSizePx}-${maxSizePx}: clamp(
    ${minSize}rem, 
    ${minSize}rem + ${maxSize - minSize} * (100vw - ${minScreenWidth}rem) / ${maxScreenWidth - minScreenWidth},
    ${maxSize}rem);`
  console.log(css)
}

const values = [
  { minSizePx: 16, maxSizePx: 18 },
  { minSizePx: 18, maxSizePx: 24 },
  { minSizePx: 22, maxSizePx: 32 },
  { minSizePx: 22, maxSizePx: 23 },
  { minSizePx: 22, maxSizePx: 30 },
  { minSizePx: 24, maxSizePx: 40 },
  { minSizePx: 24, maxSizePx: 29 },
  { minSizePx: 24, maxSizePx: 26 },
  { minSizePx: 25, maxSizePx: 31 },
  { minSizePx: 28, maxSizePx: 56 },
  { minSizePx: 28, maxSizePx: 40 },
  { minSizePx: 29, maxSizePx: 38 },
  { minSizePx: 31, maxSizePx: 38 },
  { minSizePx: 31, maxSizePx: 44 },
  { minSizePx: 32, maxSizePx: 80 },
  { minSizePx: 32, maxSizePx: 48 },
  { minSizePx: 34, maxSizePx: 62 },
  { minSizePx: 35, maxSizePx: 80 },
  { minSizePx: 35, maxSizePx: 45 },
  { minSizePx: 117, maxSizePx: 130 },
]

const css = values
.sort((a,b) => a.minSizePx - b.minSizePx)
.reduce((arr, next) => arr.find(({minSizePx, maxSizePx }) => minSizePx === next.minSizePx && maxSizePx === next.maxSizePx) ? arr : [...arr, next], [])
.map(({minSizePx, maxSizePx}) => calculateFluidStyle(minSizePx, maxSizePx))

