/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-console */
/* eslint-disable no-continue */
/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
import { createWriteStream } from 'fs'
import * as cheerio from 'cheerio'

const options = {
  headers: new Headers({
    Authorization: 'Basic ' + btoa('OenS:@Weesper113'),
    'Content-Type': 'application/x-www-form-urlencoded',
  }),
}

const getPagesFromSitemap = async (sitemapUrl, env) => {
  const result = await fetch(sitemapUrl, env === 'acc' ? options : undefined)

  if (result.status < 200 || result.status >= 300) {
    console.log('Fetch failed, you probably need to connect through VPN')
  }

  const xml = await result.text()
  const $ = cheerio.load(xml)
  const pages = $('loc')
    .map((_, value) => $(value).text())
    .toArray()
  return pages
}

const getImagesFromPage = async (pageUrl, env) => {
  const result = await fetch(pageUrl, env === 'acc' ? options : undefined)
  const html = await result.text()
  const $ = cheerio.load(html)
  const images = $('img')
    .map((_, value) => $(value).attr('src'))
    .toArray()
  return images
}

const checkImages = async (env) => {
  const sitemapUrl =
    env === 'prod'
      ? 'https://onderzoek.amsterdam.nl/sitemap.xml'
      : 'https://acc.onderzoek.amsterdam.nl/sitemap.xml'

  const today = new Date().toISOString().split('T')[0]
  const fileName = `./broken-images-${today}.csv`
  const fileWriter = createWriteStream(fileName)
  fileWriter.write('page;link;status\n')

  const { protocol, hostname, port } = new URL(sitemapUrl)
  const baseUrl = port
    ? `${protocol}//${hostname}:${port}/`
    : `${protocol}//${hostname}/`
  const pages = await getPagesFromSitemap(sitemapUrl, env)
  const allImages = []
  for (const page of pages) {
    const images = await getImagesFromPage(page, env)
    for (const image of images) {
      if (allImages.includes(image)) continue
      const url = image.startsWith('http') ? image : baseUrl + image
      allImages.push(image)

      try {
        const { status } = await fetch(url, env === 'acc' ? options : undefined)
        if (status < 200 || status >= 300) {
          fileWriter.write(`${page};${image};${status}\n`)
          console.log(page, image, status)
        }
      } catch (error) {
        fileWriter.write(`${page};${image};${error}\n`)
        console.log(page, image, error)
      }
    }
  }
}

const [env = 'prod'] = process.argv.slice(2)
checkImages(env)
