/* eslint-disable no-console */
/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
/* eslint-disable import/no-extraneous-dependencies */
import fs from 'fs'
import lighthouse from 'lighthouse'
import { launch } from 'chrome-launcher'

const colorLog = (text1, text2, color = 'blue', eol = '\r') => {
  const colors = {
    red: 31,
    green: 32,
    blue: 34,
  }
  const colorCode = colors[color] || 34
  process.stdout.write(
    ['\x1b[', colorCode, 'm', text1, '\x1b[0m', text2, eol].join(''),
  )
}

const getPages = async (env, scope) => {
  const prefix = env === 'prod' ? '' : 'acc.'
  const baseUrl = `https://${prefix}onderzoek.amsterdam.nl/`

  const urlContentPages = `${baseUrl}api/search-content`
  const responseContentPages = await fetch(urlContentPages)
  const contentPages = await responseContentPages.json()

  const urlThemes = `https://${prefix}cms.onderzoek-en-statistiek.nl/api/themes`
  const responseThemes = await fetch(urlThemes)
  const themeData = await responseThemes.json()
  const themeSlugs = themeData.data.map((d) => d.slug)

  const contentTypes = [
    { nl: 'artikel', en: 'article' },
    { nl: 'publicatie', en: 'publication' },
    { nl: 'video', en: 'video' },
    { nl: 'interactief', en: 'interactive' },
    { nl: 'dataset', en: 'dataset' },
    { nl: 'dossier', en: 'collection' },
  ]

  const singlePages = [
    { category: 'voorpagina', url: baseUrl },
    { category: 'zoek', url: `${baseUrl}zoek` },
  ]

  const mapApiResult = ({ type, slug }) => {
    const category = contentTypes.find((d) => d.en === type).nl
    return {
      category,
      url: `${baseUrl}${category}/${slug}`,
    }
  }

  let pages = []

  switch (scope) {
    case 'all':
      pages = [
        ...singlePages,
        ...themeSlugs.map((slug) => ({
          category: 'thema',
          url: `${baseUrl}thema/${slug}`,
        })),
        ...contentPages.map(mapApiResult),
      ]
      break
    case 'article':
    case 'publication':
    case 'video':
    case 'interactive':
    case 'dataset':
    case 'collection':
      pages = contentPages.filter((d) => d.type === scope).map(mapApiResult)
      break
    case 'selection':
      pages = [
        ...singlePages,
        {
          category: 'thema',
          url: `${baseUrl}thema/${themeSlugs[0]}`,
        },
        ...contentTypes.map(({ nl, en }) => ({
          category: nl,
          url: `${baseUrl}${nl}/${
            contentPages.find((d) => d.type === en).slug
          }`,
        })),
      ]
      break
    default:
      colorLog(
        'Error: ',
        `provide valid scope, e.g. 'article', 'video', 'collection', 'all', etc.`,
        'red',
        '\n',
      )
  }

  return pages
}

const runLightHouse = async (chrome, { category, url }, logLevel) => {
  const user = 'OenS'
  const pass = '@Weesper113'
  const options = {
    output: [],
    onlyCategories: ['performance', 'accessibility', 'seo', 'best-practices'],
    port: chrome.port,
    throttlingMethod: 'provided',
    throttling: {
      throughputKbps: 8000,
      downloadThroughputKbps: 8000,
      uploadThroughputKbps: 2000,
    },
    extraHeaders: {
      Authorization: `Basic ${Buffer.from(`${user}:${pass}`, 'binary').toString(
        'base64',
      )}`,
    },
  }
  const runnerResult = await lighthouse(url, options)
  const { categories } = runnerResult.lhr
  const performance = Math.round(categories.performance.score * 100)
  const accessibility = Math.round(categories.accessibility.score * 100)
  const seo = Math.round(categories.seo.score * 100)
  const bestPractices = Math.round(categories['best-practices'].score * 100)

  if (logLevel === 'verbose') {
    colorLog(url, '', 'blue', '\n')
    console.log(`category: ${category}`)
    console.log(`performance: ${performance}`)
    console.log(`accessibility: ${accessibility}`)
    console.log(`seo: ${seo}`)
    console.log(`best practices: ${bestPractices}`)
  }

  return `${category};${url};${performance};${accessibility};${seo};${bestPractices}`
}

;(async () => {
  const [env = 'acc', scope = 'selection', logLevel] = process.argv.slice(2)
  const pages = await getPages(env, scope)
  const csv = ['category;url;performance;accessibility;seo;best-practices']
  const today = new Date().toISOString().split('T')[0]

  const chrome = await launch({
    chromeFlags: ['--headless', '--throttling-method=devtools'],
  })

  for (const [i, page] of pages.entries()) {
    if (logLevel !== 'verbose')
      colorLog(`Checking ${i + 1} from ${pages.length}: `, page.url)
    const score = await runLightHouse(chrome, page, logLevel)
    csv.push(score)
    fs.writeFileSync(
      `./lighthouse-scores-${env}-${scope}-${today}.csv`,
      csv.join('\n'),
    )
  }
  colorLog(
    'Finished! ',
    `Calculated and saved scores for ${pages.length} pages`,
    'green',
    '\n',
  )

  await chrome.kill()
})()
